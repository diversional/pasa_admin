import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rtdb_repo/notif_repo.dart';

import 'package:equatable/equatable.dart';

part 'notif_state.dart';

class NotifCubit extends Cubit<NotifState> {
  NotifRepo notifRepo = NotifRepo();

  NotifCubit({required this.notifRepo}) : super(NotifState());

  Future<void> retrieveToken() async {
    final token = await notifRepo.getToken();
    emit(state.copyWith(token: token));
  }

  Future<void> requestNotification() async {
    final granted = await notifRepo.requestNotificationPermission();
    emit(state.copyWith(permissionGranted: granted));
  }

  Future<void> sendNotificationToToken(String title, String body, String token) async =>
      await notifRepo.sendNotificationToToken(title, body, token);

}
