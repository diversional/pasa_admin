
part of 'notif_cubit.dart';

class NotifState extends Equatable {

  final String? token;
  final bool? permissionGranted;

  NotifState({this.token, this.permissionGranted});

  @override
  List<Object?> get props => [token, permissionGranted];

  NotifState copyWith({
    String? token,
    bool? permissionGranted,
  }) {
    return NotifState(
        token: token ?? this.token,
        permissionGranted: permissionGranted ?? this.permissionGranted
    );
  }

}