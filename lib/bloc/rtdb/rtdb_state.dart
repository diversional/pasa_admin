part of 'rtdb_cubit.dart';

class RealtimeDatabaseState extends Equatable {

  final Admin? admin;
  final List<Patient>? patientList;
  final List<PendingRoomAlert>? pendingRoomAlerts, historyPendingRoomAlerts;
  final List<RequestMaintenance>? technicianMaintenanceRequestList;
  final List<RequestMaintenance>? housekeepingMaintenanceRequestList;
  final List<Room>? roomList;
  final List<ChatMessage>? messageList;
  final List<ChatMessage>? messageChange;
  final Patient? findPatientByRoomNumberResult;
  final String? errorMessage;

  const RealtimeDatabaseState({
    this.admin,
    this.patientList,
    this.pendingRoomAlerts,
    this.historyPendingRoomAlerts,
    this.technicianMaintenanceRequestList,
    this.housekeepingMaintenanceRequestList,
    this.roomList,
    this.messageList,
    this.messageChange,
    this.findPatientByRoomNumberResult,
    this.errorMessage,
  });

  RealtimeDatabaseState copyWith({
    Admin? admin,
    List<Patient>? patientList,
    List<PendingRoomAlert>? pendingRoomAlerts,
    List<PendingRoomAlert>? historyPendingRoomAlerts,
    List<RequestMaintenance>? technicianMaintenanceRequestList,
    List<RequestMaintenance>? housekeepingMaintenanceRequestList,
    List<Room>? roomList,
    List<ChatMessage>? messageList,
    List<ChatMessage>? messageChange,
    Patient? findPatientByRoomNumberResult,
    String? errorMessage,
  }) {
    return RealtimeDatabaseState(
      admin: admin ?? this.admin,
      patientList: patientList ?? this.patientList,
      pendingRoomAlerts: pendingRoomAlerts ?? this.pendingRoomAlerts,
      historyPendingRoomAlerts: historyPendingRoomAlerts ?? this.historyPendingRoomAlerts,
      technicianMaintenanceRequestList: technicianMaintenanceRequestList ?? this.technicianMaintenanceRequestList,
      housekeepingMaintenanceRequestList: housekeepingMaintenanceRequestList ?? this.housekeepingMaintenanceRequestList,
      roomList: roomList ?? this.roomList,
      messageList: messageList ?? this.messageList,
      messageChange: messageChange ?? this.messageChange,
      findPatientByRoomNumberResult: findPatientByRoomNumberResult ?? this.findPatientByRoomNumberResult,
      errorMessage: errorMessage ?? this.errorMessage,
    );
  }

  @override
  List<Object?> get props => [
    admin,
    patientList,
    pendingRoomAlerts,
    historyPendingRoomAlerts,
    technicianMaintenanceRequestList,
    housekeepingMaintenanceRequestList,
    roomList,
    messageList,
    messageChange,
    findPatientByRoomNumberResult,
    errorMessage,
  ];
  
}