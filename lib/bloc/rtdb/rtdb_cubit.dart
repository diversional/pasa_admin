import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:rtdb_repo/models/admin.dart';
import 'package:rtdb_repo/models/chat_message.dart';

import 'package:rtdb_repo/models/patient.dart';
import 'package:rtdb_repo/models/pending_room_alert.dart';
import 'package:rtdb_repo/models/request_maintenance.dart';
import 'package:rtdb_repo/models/room.dart';
import 'package:rtdb_repo/rtdb_repo.dart';

import 'package:bloc/bloc.dart';

part 'rtdb_state.dart';

class RealtimeDatabaseCubit extends Cubit<RealtimeDatabaseState> {
  final RealtimeDatabaseRepo _rtdbRepo;

  StreamSubscription<List<Room>>? _allRoomsSub;
  StreamSubscription<List<Patient>>? _allPatientStreamSub;
  StreamSubscription<List<PendingRoomAlert>>? _requestAlertsStreamSub;
  StreamSubscription<List<ChatMessage>>? _chatRoomStreamSub;

  StreamSubscription<List<RequestMaintenance>>? _techMaintenanceStreamSub,
      _housekeepingMaintenanceStreamSub;

  RealtimeDatabaseCubit(this._rtdbRepo) : super(const RealtimeDatabaseState()) {
    listenAllRoom();
    listenPatientsRoot();
    listenHistoryPendingRoomAlerts();
    listenTechMaintenanceRequests();
  }

  // Room
  listenAllRoom() {
    _allRoomsSub = _rtdbRepo.allRoomsStream.listen((result) {
      emit(state.copyWith(roomList: result));
    });
  }

  cancelListenAllRooms() => _allRoomsSub?.cancel();

  Future<bool> deleteRoom(String roomNumber) async {
    return await _rtdbRepo.canDeleteRoom(roomNumber);
  }

  // Room

  // Patient
  listenPatientsRoot() {
    _allPatientStreamSub = _rtdbRepo.allPatientsStream.listen((result) {
      emit(state.copyWith(patientList: result));
    });
  }

  cancelListenPatientsRoot() => _allPatientStreamSub?.cancel();

  getPatientByRoomNumber(String roomNumber) {
    _rtdbRepo.getPatientByRoomNumber(roomNumber).then((patientResult) {
      emit(state.copyWith(findPatientByRoomNumberResult: patientResult));
    }).onError((error, stackTrace) {
      emit(state.copyWith(errorMessage: error.toString()));
    });
  }

  // Patient

  // Chat
  listenChatRoom(String adminAuthId, String patientId) {
    _chatRoomStreamSub =
        _rtdbRepo.getChatRoom(adminAuthId, patientId).listen((result) {
      emit(state.copyWith(messageList: result));
    });
  }

  cancelListenChatRoom() => _chatRoomStreamSub?.cancel();

  observeChatRoom(String adminAuthId, String patientId) {
    _rtdbRepo.getChatRoom(adminAuthId, patientId).listen((result) {
      emit(state.copyWith(messageChange: result));
    });
  }

  // Chat

  // PendingRoomAlert
  listenPendingRoomAlerts() {
    _requestAlertsStreamSub =
        _rtdbRepo.pendingEmergencyOrAssistanceAlerts.listen((result) {
      emit(state.copyWith(pendingRoomAlerts: result));
    });
  }
  listenHistoryPendingRoomAlerts() {
    _rtdbRepo.listenHistoryPendingRoomAlert.listen((result) {
      emit(state.copyWith(historyPendingRoomAlerts: result));
    });
  }

  deleteHistoryPendingRoomAlert(int timeStamp) async {
    await _rtdbRepo.deleteHistoryPendingRoomAlert(timeStamp.toString());
  }

  cancelListenPendingRoomAlerts() => _requestAlertsStreamSub?.cancel();

  // PendingRoomAlert

  // Maintenance
  // technician
  listenTechMaintenanceRequests() {
    _techMaintenanceStreamSub = _rtdbRepo.technicianRequestsMaintenanceStream
        .listen((List<RequestMaintenance>? result) {
      emit(state.copyWith(technicianMaintenanceRequestList: result ?? []));
    });
  }

  cancelListenTechMaintenanceRequests() => _techMaintenanceStreamSub?.cancel();

  deleteTechMaintenanceRequest(RequestMaintenance requestMaintenance) async {
    await _rtdbRepo
        .deleteTechnicianRequest(requestMaintenance.timeStampAsId ?? '')
        .then((value) {
      final existingList = state.technicianMaintenanceRequestList;

      existingList?.remove(requestMaintenance);

      emit(
          state.copyWith(technicianMaintenanceRequestList: existingList ?? []));
    }).onError((error, stackTrace) {
      emit(state.copyWith(errorMessage: error.toString()));
    });
  }

  deleteTechnicianRequestByPatientId(String patientId) async {
    await _rtdbRepo.deleteTechnicianByPatientId(patientId);
  }

  // technician

  // housekeeping
  listenHousekeepingMaintenanceRequests() {
    _housekeepingMaintenanceStreamSub =
        _rtdbRepo.housekeepingRequestsMaintenanceStream.listen((result) {
      emit(state.copyWith(housekeepingMaintenanceRequestList: result));
    });
  }

  cancelListenHousekeepingMaintenanceRequests() =>
      _housekeepingMaintenanceStreamSub?.cancel();

  deleteHousekeepingMaintenanceRequest(
      RequestMaintenance requestMaintenance) async {
    await _rtdbRepo
        .deleteHousekeepingRequest(requestMaintenance.timeStampAsId ?? '')
        .then((value) {
      final existingList = state.housekeepingMaintenanceRequestList;

      existingList?.remove(requestMaintenance);

      emit(state.copyWith(
          housekeepingMaintenanceRequestList: existingList ?? []));
    }).onError((error, stackTrace) {
      emit(state.copyWith(errorMessage: error.toString()));
    });
  }

  deleteHousekeepingByPatientId(String patientId) async {
    await _rtdbRepo.deleteHousekeepingByPatientId(patientId);
  }

  // housekeeping

  // Maintenance

  // Admin
  getAdminByEmail(String email) async {
    _rtdbRepo.getAdminByEmail(email).then((value) {
      emit(state.copyWith(admin: value));
    });
  }

  // Admin

  // Write
  writeAdmin(Admin admin) {
    _rtdbRepo.writeAdmin(admin);
    emit(state.copyWith(admin: admin));
  }

  deleteAdminAndPatientConvo(String adminAuthId, String patientId) {
    _rtdbRepo.deleteAdminAndPatientConvo(adminAuthId, patientId);
  }

  deleteChatRoom(String patientId) {
    _rtdbRepo.deletePatientChatRoom(patientId);
  }

  writeHistoryPendingRoomAlert(PendingRoomAlert pendingRoomAlert) {
    _rtdbRepo.writeHistoryPendingRoomAlert(pendingRoomAlert).then((success) {
      if (!success) {
        emit(state.copyWith(
            errorMessage:
            'Failed to write send ChatMessage, check RealtimeDatabaseRepo')
        );
      }
    });
  }

  deleteRoomAlert(String patientId) {
    _rtdbRepo.deleteRoomAlert(patientId).then((success) {
      if (!success) {
        emit(state.copyWith(
            errorMessage: 'Failed to write send ChatMessage, check RealtimeDatabaseRepo'));
      }
    });
  }

  sendMessage(String adminAuthId, String patientId, ChatMessage chatMessage) {
    _rtdbRepo.sendMessage(adminAuthId, patientId, chatMessage).then((success) {
      if (success) {
        final updatedList = List<ChatMessage>.from(state.messageList ?? []);
        emit(state.copyWith(messageList: updatedList));
      } else {
        emit(state.copyWith(
            errorMessage:
                'Failed to write send ChatMessage, check RealtimeDatabaseRepo'));
      }
    });
  }

  createNewPatient(Patient patient, String id, {required bool update}) {
    _rtdbRepo.writePatient(patient, id, update: update).then((success) {
      if (success) {
        // If the patient creation was successful, update the patient list in the state
        // if (id == null) {
        //   final updatedList = List<Patient>.from(state.patientList ?? [])
        //   ..add(patient);
        //   emit(state.copyWith(patientList: updatedList));
        // }
      } else {
        emit(state.copyWith(
            errorMessage:
                'Failed to write Patient, check RealtimeDatabaseRepo'));
      }
    });
  }

  deletePatient(Patient patient) {
    _rtdbRepo.deletePatient(patient).then((success) {
      if (success) {
        final updatedList = List<Patient>.from(state.patientList ?? [])
          ..remove(patient);
        emit(state.copyWith(patientList: updatedList));
      } else {
        emit(state.copyWith(
            errorMessage:
                'Failed to write Patient, check RealtimeDatabaseRepo'));
      }
    });
  }

  createNewRoom(Room room) {
    _rtdbRepo.writeRoom(room).then((success) {
      if (!success) {
        emit(state.copyWith(
            errorMessage: 'Failed to write Room, check RealtimeDatabaseRepo'));
      }
    });
  }

  // Write

  @override
  Future<void> close() {
    cancelListenPatientsRoot();
    cancelListenPendingRoomAlerts();
    cancelListenTechMaintenanceRequests();
    cancelListenHousekeepingMaintenanceRequests();
    cancelListenChatRoom();
    return super.close();
  }
}
