part of 'credential_cubit.dart';

class CredentialState extends Equatable {
  const CredentialState({
    this.admin = Admin.empty,
    this.uid = '',
    this.name = '',
    this.email = const Email.pure(),
    this.password = const Password.pure(),
    this.formzStatus = FormzStatus.pure,
    this.adminType = AdminType.notSet,
    this.errorMessage,
  });

  final Admin admin;
  final String uid;
  final String name;
  final Email email;
  final Password password;
  final FormzStatus formzStatus;
  final AdminType adminType;
  final String? errorMessage;

  @override
  List<Object> get props => [admin, uid, name, email, password, formzStatus, adminType];

  CredentialState copyWith({
    Admin? admin,
    String? uid,
    String? name,
    Email? email,
    Password? password,
    FormzStatus? formzStatus,
    AdminType? adminType,
    String? errorMessage,
  }) {
    return CredentialState(
      admin: admin ?? this.admin,
      uid: uid ?? this.uid,
      name: name ?? this.name,
      email: email ?? this.email,
      password: password ?? this.password,
      formzStatus: formzStatus ?? this.formzStatus,
      adminType: adminType ?? this.adminType,
      errorMessage: errorMessage ?? this.errorMessage,
    );
  }
}