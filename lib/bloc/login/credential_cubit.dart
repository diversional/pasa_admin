import 'package:bloc/bloc.dart';

import 'package:auth_repo/auth_exception.dart';
import 'package:auth_repo/auth_repo.dart';
import 'package:equatable/equatable.dart';

import 'package:form_input/form_input.dart';
import 'package:formz/formz.dart';
import 'package:rtdb_repo/models/admin.dart';

part 'credential_state.dart';

class CredentialCubit extends Cubit<CredentialState> {

  final AuthRepo _authRepo;

  CredentialCubit(this._authRepo) : super(const CredentialState());

  void nameChanged(String value) {
    emit(state.copyWith(
      name: value
    ));
  }

  void emailChanged(String value) {
    final email = Email.dirty(value);
    emit(state.copyWith(
      email: email,
      formzStatus: Formz.validate([email, state.password])
    ));
  }

  void passwordChanged(String value) {
    final password = Password.dirty(value);
    emit(
      state.copyWith(
        password: password,
        formzStatus: Formz.validate([state.email, password]),
      ),
    );
  }

  void setAdmin(Admin admin) {
    emit(
      state.copyWith(
        admin: admin,
        formzStatus: Formz.validate([state.email, state.password]),
      )
    );
  }

  void resetAdminType() {
    emit(
        state.copyWith(
          adminType: AdminType.notSet,
        )
    );
  }

  void setAdminType(AdminType adminType) {
    emit(
      state.copyWith(
        adminType: adminType,
        formzStatus: Formz.validate([state.email, state.password]),
      )
    );
  }

  Future<void> logInWithCredentials() async {
    if (!state.formzStatus.isValidated) return;
    emit(state.copyWith(formzStatus: FormzStatus.submissionInProgress));
    try {
      await _authRepo.logInWithEmailAndPassword(
        email: state.email.value,
        pass: state.password.value,
      );
      emit(state.copyWith(formzStatus: FormzStatus.submissionSuccess));
    } on AuthLoginException catch (e) {
      emit(
        state.copyWith(
          errorMessage: e.message,
          formzStatus: FormzStatus.submissionFailure,
        ),
      );
    } catch (_) {
      emit(state.copyWith(formzStatus: FormzStatus.submissionFailure));
    }
  }


  Future<void> registerAdmin() async {
    if (!state.formzStatus.isValidated) return;
    emit(state.copyWith(formzStatus: FormzStatus.submissionInProgress));

    try {
      await _authRepo.registerWithEmailAndPassword(
        email: state.email.value,
        pass: state.password.value,
      );
      emit(state.copyWith(
        uid: _authRepo.currentUser.authId
      ));
    } on AuthLoginException catch (e) {
      emit(
        state.copyWith(
          errorMessage: e.message,
          formzStatus: FormzStatus.submissionFailure,
        ),
      );
    } catch (_) {
      emit(state.copyWith(formzStatus: FormzStatus.submissionFailure));
    }
  }

}
