part of 'auth_bloc.dart';

abstract class AuthEvent {
  const AuthEvent();
}

class AuthLogoutRequestedEvent extends AuthEvent {
  const AuthLogoutRequestedEvent();
}

class _AuthUserChangedEvent extends AuthEvent {
  
  final Admin admin;
  final String name, email;

  const _AuthUserChangedEvent(
    this.admin,
    this.name,
    this.email
  );

}
