import 'dart:async';

import 'package:auth_repo/auth_repo.dart';
import 'package:rtdb_repo/models/admin.dart';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import 'package:universal_html/html.dart' as html;

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {

  final AuthRepo _authRepo;
  late final StreamSubscription<Admin> _adminSubscription;

  AuthBloc({required AuthRepo authRepo})
      : _authRepo = authRepo,
        super(
          authRepo.currentUser.isEmpty
            ? const AuthState.unauthed()
            : AuthState.authed(
              authRepo.currentUser,
              authRepo.currentUser.name,
              authRepo.currentUser.email,
            )
        ) {
    on<_AuthUserChangedEvent>(_onAdminChanged);
    on<AuthLogoutRequestedEvent>(_onLogoutRequested);
    _adminSubscription = _authRepo.user.listen((user) {
      
      add(_AuthUserChangedEvent(user, user.name ?? '', user.email ?? ''));
    });
  }

  _onAdminChanged(_AuthUserChangedEvent event, Emitter<AuthState> emit) {
    emit(
      event.admin.isEmpty == false
        ?
        AuthState.authed(
          event.admin,
          event.name,
          event.email
        )
        :
        const AuthState.unauthed()
    );
  }

  _onLogoutRequested(AuthLogoutRequestedEvent event, Emitter<AuthState> emit) async {
    await _authRepo.logOut();
    html.window.location.reload();
  }

  @override
  Future<void> close() {
    _adminSubscription.cancel();
    return super.close();
  }

}
