part of 'auth_bloc.dart';

enum AuthStatus { authed, unauthed }

class AuthState extends Equatable {

  final AuthStatus authStatus;
  final String name, email;
  final Admin admin;

  const AuthState._({
    required this.authStatus,
    this.name = '',
    this.email = '',
    this.admin = Admin.empty,
  });

  const AuthState.authed(Admin admin, String? name, String? email)
      : this._(
        authStatus: AuthStatus.authed,
        name: name ?? '',
        email: email ?? '',
        admin: admin
      );

  const AuthState.unauthed() : this._(authStatus: AuthStatus.unauthed);

  @override
  List<Object?> get props => [authStatus, name, email, admin];
}
