import 'package:flutter/material.dart';

import '../constants/constants.dart';


class PopupMenuOption extends StatelessWidget {

  final bool horizontalIcon;
  final Function()? onDetailsTap,
      onChatTap,
      onUpdateTap,
      onDeleteTap,
      onClearChat;

  const PopupMenuOption({
    this.horizontalIcon = false,
    this.onDetailsTap,
    this.onChatTap,
    this.onUpdateTap,
    this.onDeleteTap,
    this.onClearChat,
    super.key
  });

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<String>(
      icon: horizontalIcon ?  const Icon(Icons.more_horiz_rounded) :  const Icon(Icons.more_vert_rounded),
      initialValue: Constant().popupMenuItems[0],
      onSelected: (value) {
          
        switch (value) {
          case 'Details':
            if (onDetailsTap != null) {
              onDetailsTap!();
            }
            break;
          case 'Chat':
            if (onChatTap != null) {
              onChatTap!();
            }
            break;
          case 'Update':
            if (onUpdateTap != null) {
              onUpdateTap!();
            }
            break;
          case 'Delete':
            if (onDeleteTap != null) {
              onDeleteTap!();
            }
            break;
          case 'Clear Conversation':
            if (onClearChat != null) {
              onClearChat!();
            }
        }
      },
      itemBuilder: (context) {
        return Constant().popupMenuItems
          .where((entry) => _detectCallbackIsNotNull(entry))
          .map((entry) => PopupMenuItem<String>(
            value: entry,
            child: Text(entry),
          ))
          .toList();
      },
    );
  }

  bool _detectCallbackIsNotNull(String value) {

    switch (value) {
      case 'Details':
        return onDetailsTap != null;
      case 'Chat':
        return onChatTap != null;
      case 'Update':
        return onUpdateTap != null;
      case 'Delete':
        return onDeleteTap != null;
      case 'Clear Conversation':
        return onClearChat != null;
    }

    return false;
  }

}