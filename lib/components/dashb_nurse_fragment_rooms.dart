import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pasa_admin/bloc/rtdb/rtdb_cubit.dart';
import 'package:pasa_admin/components/dialog_add_new_room.dart';

import 'package:pasa_admin/components/button_rounded_yellow.dart';
import 'package:pasa_admin/components/dialog_emergency.dart';
import 'package:pasa_admin/components/popup_menu_option.dart';
import 'package:rtdb_repo/models/pending_room_alert.dart';
import 'package:rtdb_repo/models/room.dart';
import 'package:rtdb_repo/models/room_alert_type.dart';

import '../constants/constants.dart';
import 'dialog_assistance_input.dart';
import 'dialog_delete_confirmation.dart';
import 'dialog_details.dart';

class DashbNurseFragmentRoom extends StatelessWidget {

  final List<Room> _roomsList;
  final List<PendingRoomAlert>? _pendingRoomAlerts;
  final Function() onConfirm;

  const DashbNurseFragmentRoom({
    required List<Room> roomsList,
    required List<PendingRoomAlert>? pendingRoomAlerts,
    required this.onConfirm,
    super.key
  })
      : _roomsList = roomsList,
        _pendingRoomAlerts = pendingRoomAlerts;

  @override
  Widget build(BuildContext context) {
    final sortedRoomsList = _sortRoomsByFloor(_roomsList);

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 10.0),
        Expanded(
          child: GridView.count(
            crossAxisCount: 9,
            children: List.generate(sortedRoomsList.length,
                (index) => _itemWidgetRoom(sortedRoomsList[index], context)
            ),
          ),
        ),
        const SizedBox(height: 10.0),
        ButtonRoundedYellow(
          text: 'ADD NEW ROOM',
          pressListener: () {
            showDialog(
                context: context, builder: (context) => DialogAddNewRoom());
          },
        )
      ],
    );
  }


  List<Room> _sortRoomsByFloor(List<Room> rooms) {
    rooms.sort((a, b) => a.floor!.compareTo(b.floor!));
    return rooms;
  }

  _itemWidgetRoom(Room room, BuildContext context) {
    final isAlerting = _pendingRoomAlerts
        ?.map((roomAlerts) => roomAlerts.roomNumber)
        .toList()
        .contains(room.number);

    final roomToPendingRoomAlert = _getPendingRoomAlertByRoomModel(room);
    final typeOfAlert = roomToPendingRoomAlert?.roomAlertType;

    Color? bgColor =
        (isAlerting != null && isAlerting && roomToPendingRoomAlert != null)
            ? _getBgColorByAlertType(typeOfAlert!)
            : null;

    return GestureDetector(
      onTap: () {
        switch (typeOfAlert) {
          case 0: // Assistance
            showDialog(
              context: context,
              builder: (context) => DialogAssistanceInput(
                pendingRoomAlerts: _pendingRoomAlerts!,
                onConfirm: () {
                  onConfirm();
                },
              ),
            );
            break;
          case 1: // Emergency
            showDialog(
              context: context,
              builder: (context) => DialogEmergency(
                pendingRoomAlerts: _pendingRoomAlerts!,
                onConfirm: () {
                  onConfirm();
                },
              ),
            );
            break;
        }
      },
      child: Wrap(
        children: [
          Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black,
                  width: 1.0,
                ),
              ),
              child: FlashingContainer(
                color: bgColor,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    PopupMenuOption(
                      horizontalIcon: true,
                      onDetailsTap: () {
                        showDialog(
                          context: context,
                          builder: (context) {
                            return DialogDetails(
                                children: [
                                  Text('Room #: ${room.number}'),
                                  const SizedBox(height: 10.0),
                                  Text('Bed Capacity: ${room.bedCount}'),
                                  const SizedBox(height: 10.0),
                                  Text('Floor: ${room.floor}'),
                                ]
                            );
                          }
                        );
                      },
                      onDeleteTap: () {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return DialogDeleteConfirmation(
                              name: 'room ${room.number}',
                            );
                          },
                        ).then((value) async {
                          if (value) {
                            final canBeDeleted = await context.read<RealtimeDatabaseCubit>()
                                .deleteRoom(room.number ?? '');
                            if (!canBeDeleted) {
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(
                                  const SnackBar(content: Text('Cannot delete a room that has a patient'))
                              );
                            }
                          }
                        });

                      },
                    ),
                    Text(
                      'ROOM\n${room.floor} - ${room.number}',
                      style: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ))
        ],
      ),
    );
  }

  Color? _getBgColorByAlertType(int typeOfAlert) {
    final enumPendingAlertType = _getRoomAlertType(typeOfAlert);

    if (enumPendingAlertType == null) {
      return null;
    }

    switch (enumPendingAlertType) {
      case PendingRoomAlertType.assistance:
        return Colors.yellow;
      case PendingRoomAlertType.emergency:
        return Colors.red;
    }
  }

  PendingRoomAlertType? _getRoomAlertType(flag) {
    switch (flag) {
      case 0:
        return PendingRoomAlertType.assistance;
      case 1:
        return PendingRoomAlertType.emergency;
      default:
        return null;
    }
  }

  PendingRoomAlert? _getPendingRoomAlertByRoomModel(Room room) {
    if (_pendingRoomAlerts == null) {
      return null;
    }

    for (PendingRoomAlert alert in _pendingRoomAlerts!) {
      if (alert.roomNumber == room.number) {
        return alert;
      }
    }

    return null;
  }
}

class FlashingContainer extends StatefulWidget {
  final Color? color;
  final Widget? child;

  const FlashingContainer({Key? key, this.color, this.child}) : super(key: key);

  @override
  _FlashingContainerState createState() => _FlashingContainerState();
}

class _FlashingContainerState extends State<FlashingContainer> {
  final audioPlayer = AudioPlayer();
  Color? _bgColor;

  late Timer? _timer;

  @override
  void initState() {
    super.initState();

    if (mounted) {
      _bgColor = widget.color;

      _timer = Timer.periodic(const Duration(milliseconds: 500), (Timer t) {
        _changeBgColor();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_bgColor != null) {
      _playAudio();
    }

    return Center(
      child: Container(
        color: _bgColor,
        child: widget.child,
      ),
    );
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  void _changeBgColor() {
    setState(() {
      _bgColor = _bgColor == null ? widget.color : null;
    });
  }

  void _playAudio() async {
    await audioPlayer.play(UrlSource(Constant().alertSound));
  }
}
