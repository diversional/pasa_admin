import 'package:flutter/material.dart';
import '../constants/constants.dart';

class DashbLabel extends StatelessWidget {
  final String title;

  const DashbLabel({required this.title, super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      width: 100,
      decoration: BoxDecoration(
          color: Constant().pasaBlueColor,
          borderRadius: const BorderRadius.all(Radius.circular(10))),
      child: Center(
        child: Text(
          title,
          style: const TextStyle(fontSize: 42, color: Colors.white),
        ),
      ),
    );
  }
}
