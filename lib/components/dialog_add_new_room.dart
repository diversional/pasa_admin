import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pasa_admin/bloc/rtdb/rtdb_cubit.dart';
import 'package:rtdb_repo/models/room.dart';

import 'button_rounded_yellow.dart';
import 'textfield_rounded.dart';

class DialogAddNewRoom extends StatelessWidget {

  final TextEditingController roomNumberController = TextEditingController();
  final TextEditingController bedCountController = TextEditingController();
  final TextEditingController floorController = TextEditingController();

  DialogAddNewRoom({super.key});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadiusDirectional.zero,
      ),
      child: Container(
        width: MediaQuery.of(context).size.width * 0.8,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextFieldRounded(
                numOnly: true,
                textStart: 'Room Number',
                bgColor: const Color(0xffB7DBC3),
                textController: roomNumberController,
              ),
              const SizedBox(height: 10.0),
              TextFieldRounded(
                numOnly: true,
                textStart: 'Bed Count',
                bgColor: const Color(0xffB7DBC3),
                textController: bedCountController,
              ),
              const SizedBox(height: 10.0),
              TextFieldRounded(
                numOnly: true,
                textStart: 'Floor',
                bgColor: const Color(0xffB7DBC3),
                textController: floorController,
              ),
              const SizedBox(height: 10.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ButtonRoundedYellow(
                    text: 'CLEAR ALL',
                    pressListener: () => _clearAll(context),
                  ),
                  const SizedBox(width: 10.0),
                  ButtonRoundedYellow(
                    text: 'SAVE',
                    pressListener: () {
                      String roomNumber = roomNumberController.text;
                      int bedCount = int.tryParse(bedCountController.text) ?? 0;
                      int floor = int.tryParse(floorController.text) ?? 0;

                      if (roomNumber.isEmpty || bedCount == 0 || floor == 0) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text('Please fill out all fields')),
                        );
                        return;
                      }

                      context.read<RealtimeDatabaseCubit>().createNewRoom(
                          Room(
                              number: roomNumber,
                              bedCount: bedCount,
                              floor: floor
                          )
                      );

                      Navigator.of(context).pop();
                    },
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  _clearAll(context) {
    roomNumberController.clear();
    bedCountController.clear();
    floorController.clear();

    Navigator.of(context).pop();
  }

}
