import 'package:audioplayers/audioplayers.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pasa_admin/bloc/rtdb/rtdb_cubit.dart';
import 'package:pasa_admin/components/dialog_delete_confirmation.dart';
import 'package:pasa_admin/cookie_manager.dart';
import 'package:pasa_admin/models/chat_cookie.dart';
import 'package:rtdb_repo/models/chat_message.dart';
import 'package:rtdb_repo/models/patient.dart';

import '../constants/constants.dart';
import '../pages/chat_page.dart';
import 'button_rounded_yellow.dart';
import 'dashb_component_table_listview.dart';
import 'dashb_component_table_property.dart';
import 'dialog_add_new_patient.dart';
import 'dialog_details.dart';
import 'popup_menu_option.dart';

class DashbNurseFragmentPatients extends StatelessWidget {

  final List<Patient> patientsList;
  final List<ChatMessage>? observeChatMessage;
  final Function(Patient) onTap;
  final Function(Patient)? onNewMessage;

  const DashbNurseFragmentPatients({
    required this.patientsList,
    this.observeChatMessage,
    required this.onTap,
    this.onNewMessage,
    super.key
  });

  @override
  Widget build(BuildContext context) {

    print('DashbNurseFragmentPatients stless build patientsList= ${patientsList.length}');

    final uid = FirebaseAuth.instance.currentUser!.uid;

    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        const DashbComponentTableProperty(
          properties: ['Room #', 'Name', 'Bed', 'More'],
        ),
        DashbComponentTableListView(
          listLength: patientsList.length,
          children: (index) {
            final patient = patientsList[index];
            final patientId = patient.id;

            final patientMessageList = observeChatMessage?.where((chatMessage) {
              return chatMessage.senderAssignedId == patientId;
            }).toList();

            // Cache

            // Cache

            return
              patientMessageList == null ?
                  const CircularProgressIndicator()
                  :
              NotifiedItem(
                patient: patient,
                filteredChatMessageLength: patientMessageList.length,
                adminAuthId: uid,
                onTap: onTap,
                onNewMessage: onNewMessage,
              );

          },
        ),
        const SizedBox(height: 10.0),
        ButtonRoundedYellow(
          text: 'ADD NEW PATIENT',
          pressListener: () {
            showDialog(
                context: context, builder: (context) => DialogAddNewPatient()
            );
          },
        )
      ],
    );
  }
}

class NotifiedItem extends StatelessWidget {

  final Patient patient;
  final int filteredChatMessageLength;
  final String adminAuthId;
  final Function(Patient) onTap;
  final Function(Patient)? onNewMessage;

  NotifiedItem({
    required this.patient,
    required this.filteredChatMessageLength,
    required this.adminAuthId,
    required this.onTap,
    this.onNewMessage,
    super.key,
  });

  final audioPlayer = AudioPlayer();

  late int chatCookiedCount;

  @override
  Widget build(BuildContext context) {
    final patientId = patient.id;

    final chatCookieKey = '$patientId$adminAuthId';
    final chatCookieStr = CookieManager.getCookie(chatCookieKey);

    if (chatCookieStr.isEmpty) {
      CookieManager.addToCookie(
          chatCookieKey,
          ChatCookie(
              keyAndSenderPatientId: chatCookieKey,
              messageLength: 0
          ).toStringJson()
      );
      chatCookiedCount = 0;
    } else {
      chatCookiedCount = ChatCookie.fromJson(ChatCookie.toMapJson(chatCookieStr)).messageLength ?? 0;

      if (chatCookiedCount < filteredChatMessageLength) {
        CookieManager.addToCookie(
            chatCookieKey,
            ChatCookie(
                keyAndSenderPatientId: chatCookieKey,
                messageLength: filteredChatMessageLength
            ).toStringJson()
        );
        _playRadioButtonTone();

        if (onNewMessage != null) {
          onNewMessage!(patient);
        }
      }
    }

    if (chatCookiedCount < filteredChatMessageLength) {
      return InkWell(
        onTap: () {
          onTap(patient);
        },
        child: Container(
          color: Colors.grey[200],
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(patient.roomNumber ?? ''),
              Text(patient.name ?? ''),
              Text('${patient.bedNumber}'),
              PopupMenuOption(
                onDetailsTap: () {
                  showDialog(
                      context: context,
                      builder: (context) => DialogDetails(
                          children: patient
                              .toReadableProperties()
                              .entries
                              .where((entry) => entry.value != null)
                              .map((entry) {
                            return Column(
                              children: [
                                Row(
                                  children: [
                                    const SizedBox(width: 10.0),
                                    Text('${entry.key}:'),
                                    const SizedBox(width: 10.0),
                                    Flexible(
                                      child: Text(
                                        '${entry.value}',
                                        style: const TextStyle(
                                            overflow: TextOverflow.ellipsis),
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 10.0)
                              ],
                            );
                          }).toList()));
                },
                onChatTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) {
                      return ChatPage(
                        patientId: patient.id,
                        notifToken: patient.notifToken,
                      );
                    }),
                  );
                },
                onUpdateTap: () {
                  showDialog(
                      context: context,
                      builder: (context) => DialogAddNewPatient(
                        toUpdatePatient: patient,
                      ));
                },
                onDeleteTap: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return DialogDeleteConfirmation(
                        name: 'patient ${patient.name}',
                      );
                    },
                  ).then((value) {
                    if (value) {
                      final patientId = patient.id;
                      context
                          .read<RealtimeDatabaseCubit>()
                          .deleteTechnicianRequestByPatientId(patientId);
                      context
                          .read<RealtimeDatabaseCubit>()
                          .deleteHousekeepingByPatientId(patientId);
                      context
                          .read<RealtimeDatabaseCubit>()
                          .deleteChatRoom(patientId);
                      context
                          .read<RealtimeDatabaseCubit>()
                          .deletePatient(patient);
                      CookieManager.addToCookie(
                          chatCookieKey,
                          ChatCookie(
                              keyAndSenderPatientId: chatCookieKey,
                              messageLength: 0
                          ).toStringJson()
                      );
                    }
                  });
                },
                onClearChat: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return DialogDeleteConfirmation(
                        name: 'patient ${patient.name}\'s conversation',
                      );
                    },
                  ).then((value) {
                    if (value) {
                      context
                          .read<RealtimeDatabaseCubit>()
                          .deleteAdminAndPatientConvo(adminAuthId, patient.id);
                      ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text('Conversation cleared'))
                      );
                    }
                  });
                },
              )
            ],
          ),
        ),
      );
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(patient.roomNumber ?? ''),
          Text(patient.name ?? ''),
          Text('${patient.bedNumber}'),
          PopupMenuOption(
            onDetailsTap: () {
              showDialog(
                  context: context,
                  builder: (context) => DialogDetails(
                      children: patient
                          .toReadableProperties()
                          .entries
                          .where((entry) => entry.value != null)
                          .map((entry) {
                        return Column(
                          children: [
                            Row(
                              children: [
                                const SizedBox(width: 10.0),
                                Text('${entry.key}:'),
                                const SizedBox(width: 10.0),
                                Flexible(
                                  child: Text(
                                    '${entry.value}',
                                    style: const TextStyle(
                                        overflow: TextOverflow.ellipsis),
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(height: 10.0)
                          ],
                        );
                      }).toList()));
            },
            onChatTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) {
                  return ChatPage(
                    patientId: patient.id,
                    notifToken: patient.notifToken,
                  );
                }),
              );
            },
            onUpdateTap: () {
              showDialog(
                  context: context,
                  builder: (context) => DialogAddNewPatient(
                    toUpdatePatient: patient,
                  ));
            },
            onDeleteTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return DialogDeleteConfirmation(
                    name: 'patient ${patient.name}',
                  );
                },
              ).then((value) {
                if (value) {
                  final patientId = patient.id;
                  context
                      .read<RealtimeDatabaseCubit>()
                      .deleteTechnicianRequestByPatientId(patientId);
                  context
                      .read<RealtimeDatabaseCubit>()
                      .deleteHousekeepingByPatientId(patientId);
                  context
                      .read<RealtimeDatabaseCubit>()
                      .deleteChatRoom(patientId);
                  context
                      .read<RealtimeDatabaseCubit>()
                      .deletePatient(patient);

                  CookieManager.addToCookie(
                      chatCookieKey,
                      ChatCookie(
                          keyAndSenderPatientId: chatCookieKey,
                          messageLength: 0
                      ).toStringJson()
                  );
                }
              });
            },
            onClearChat: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return DialogDeleteConfirmation(
                    name: 'patient ${patient.name}\'s conversation',
                  );
                },
              ).then((value) {
                if (value) {
                  context
                      .read<RealtimeDatabaseCubit>()
                      .deleteAdminAndPatientConvo(adminAuthId, patient.id);
                  CookieManager.addToCookie(
                      chatCookieKey,
                      ChatCookie(
                          keyAndSenderPatientId: chatCookieKey,
                          messageLength: 0
                      ).toStringJson()
                  );
                  ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('Conversation cleared')));
                }
              });
            },
          )
        ],
      ),
    );
  }

  _playRadioButtonTone() async {
    await audioPlayer.play(UrlSource(Constant().radioButtonTone));
  }
}
