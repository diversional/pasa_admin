import 'package:flutter/material.dart';

class DashbComponentTableListView extends StatelessWidget {

  final int listLength;
  final Widget Function(int) children;
  Function(int)? onTapListener;

  DashbComponentTableListView({
    required this.listLength,
    required this.children,
    this.onTapListener,
    super.key
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
        itemCount: listLength,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () => (onTapListener != null) ? onTapListener!(index) : null,
            child: Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.grey[300]!,
                  width: 1.0,
                ),
              ),
              child: children(index),
            ),
          );
        },
      ),
    );
  }
}
