import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pasa_admin/components/popup_menu_option.dart';
import 'package:rtdb_repo/models/pending_room_alert.dart';

import '../bloc/rtdb/rtdb_cubit.dart';
import '../pages/chat_page.dart';
import 'dashb_component_table_listview.dart';
import 'dashb_component_table_property.dart';
import 'dialog_assistance_input.dart';
import 'dialog_delete_confirmation.dart';
import 'dialog_details.dart';
import 'dialog_emergency.dart';

import 'package:intl/intl.dart' as date_format;

class DashbNurseFragmentPendingRequests extends StatelessWidget {

  final List<PendingRoomAlert> _pendingAlertsList;

  const DashbNurseFragmentPendingRequests({
    required List<PendingRoomAlert> pendingAlertsList,
    super.key
  }) : _pendingAlertsList = pendingAlertsList;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                const DashbComponentTableProperty(
                  properties: ['Room', 'Name', 'Message', 'Bed', 'Time & Date', 'More'],
                ),
                DashbComponentTableListView(
                  listLength: _pendingAlertsList.length ,
                  children: (index) {
                    final alert = _pendingAlertsList[index];

                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(alert.roomNumber.toString()),
                          Text(alert.name ?? ''),
                          alert.roomAlertType == 0 ?
                          Text(alert.message)
                              :
                          const Text(
                            'Emergency',
                            style: TextStyle(color: Colors.red),
                          ),
                          Text(alert.bedNumber.toString()),
                          Text(date_format.DateFormat(
                              'MM/dd hh:mm a'
                          ).format(
                              DateTime.fromMillisecondsSinceEpoch(alert.timeStamp)
                          )),
                          PopupMenuOption(
                            onDetailsTap: () {
                              showDialog(
                                  context: context,
                                  builder: (context) => DialogDetails(
                                    children: [
                                      Text('Name: ${alert.name}'),
                                      const SizedBox(height: 10.0),
                                      Text('Room: ${alert.roomNumber}'),
                                      const SizedBox(height: 10.0),
                                      Text('Bed: ${alert.bedNumber}'),
                                      const SizedBox(height: 10.0),
                                      Text('Alert Type: ${
                                          alert.roomAlertType == 0 ?
                                          'Assistance' : 'Emergency'
                                      }'),
                                      const SizedBox(height: 10.0),
                                      Text('Time Confirmed: ${
                                          date_format.DateFormat(
                                              'yyyy-MM-dd hh:mm:ss a'
                                          ).format(
                                              DateTime.fromMillisecondsSinceEpoch(alert.timeStamp)
                                          )
                                      }'),
                                    ],
                                  )
                              );
                            },
                            onChatTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) {
                                  return ChatPage(
                                    patientId: alert.patientId,
                                  );
                                }),
                              );
                            },
                            onDeleteTap: () {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return DialogDeleteConfirmation(
                                    name: 'this pending request from patient ${alert.name}',
                                  );
                                },
                              ).then((value) {
                                if (value) {
                                  context.read<RealtimeDatabaseCubit>()
                                      .deleteHistoryPendingRoomAlert(alert.timeStamp);
                                }
                              });
                            },
                          )
                        ],
                      ),
                    );
                  },
                ),
              ],
            ),
    );

    
  }
}
