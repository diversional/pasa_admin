import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rtdb_repo/models/pending_room_alert.dart';

import '../bloc/rtdb/rtdb_cubit.dart';

class DialogAssistanceInput extends StatelessWidget {

  final List<PendingRoomAlert> pendingRoomAlerts;
  final Function() onConfirm;

  const DialogAssistanceInput({
    required this.pendingRoomAlerts,
    required this.onConfirm,
    super.key
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.zero,
      ),
      backgroundColor: const Color(0xffFAEA7E),
      child: SizedBox(
        width: MediaQuery.of(context).size.width * 0.4,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                decoration: const BoxDecoration(
                  color: Color(0xffFAEA7E),
                ),
                child: Row(
                  children: [
                    const Expanded(
                      child: Center(
                        child: Text(
                          '! ! ! ASSISTANCE ! ! !',
                          style: TextStyle(
                              fontSize: 24, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    IconButton(
                      icon: const Icon(Icons.close),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                ),
              ),
            ),
            Container(
              decoration: const BoxDecoration(
                color: Color(0xffE4C379),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Patient\'s Information',
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const SizedBox(height: 10.0),
                          ListView(
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              children: _supplyText()
                          )
                        ],
                      ),
                    ),
                  ),
                  Flexible(
                    fit: FlexFit.tight,
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          (pendingRoomAlerts.length == 1) ?
                          Text(
                            'ROOM: ${pendingRoomAlerts.first.roomNumber}',
                            style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                          ): const SizedBox()
                          ,
                          const SizedBox(height: 10),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.white,
                            ),
                            child: ListView(
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: pendingRoomAlerts.map((roomAlert) {

                                      if (roomAlert.roomAlertType == 0) {
                                        return Wrap(
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(0, 4.0, 0, 4.0),
                                              child: Text(
                                                  'Room ${roomAlert.roomNumber}: ${roomAlert.message}',
                                                  style: const TextStyle(fontWeight: FontWeight.bold)
                                              ),
                                            )
                                          ],
                                        );
                                      } else {
                                        return const SizedBox();
                                      }

                                    }).toList(),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(height: 10),
                          Wrap(
                            children: _supplyConfirmButton(context),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  List<Widget> _supplyText() {
    return pendingRoomAlerts.map((entry) {
      if (entry.roomAlertType == 0) {
        return Padding(
          padding: const EdgeInsets.fromLTRB(0, 0, 8.0, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Divider(
                color: Colors.black,
                height: 2,
                thickness: 1,
              ),
              const SizedBox(height: 4.0),
              Text('Name: ${entry.name}'),
              Text('Room: ${entry.roomNumber}'),
              Text('Bed: ${entry.bedNumber}'),
              const SizedBox(height: 4.0),
            ],
          ),
        );
      } else {
        return const SizedBox();
      }
    }).toList();
  }

  List<Widget> _supplyConfirmButton(BuildContext context) {
    final singleAlert = pendingRoomAlerts.length == 1;

    return pendingRoomAlerts.map((entry) {
      final patientId = entry.patientId;

      if (entry.roomAlertType == 0) {
        return Row(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 8.0, 0, 8.0),
              child: MaterialButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30)),
                onPressed: () {
                  final matchedAlertOfPatient = pendingRoomAlerts.where(
                          (element) => element.patientId == patientId
                  ).first;
                  context.read<RealtimeDatabaseCubit>().writeHistoryPendingRoomAlert(matchedAlertOfPatient);

                  context.read<RealtimeDatabaseCubit>()
                      .deleteRoomAlert(patientId);
                  Navigator.of(context).pop();
                  onConfirm();
                },
                color: const Color(0xff091B2A),
                child: Text(
                  singleAlert ? 'CONFIRM' : 'CONFIRM ${entry.name}',
                  style: const TextStyle(color: Colors.white),
                ),
              ),
            ),
            const SizedBox(height: 5.0, width: 5.0)
          ],
        );
      } else {
        return const SizedBox();
      }

    }).toList();
  }
}
