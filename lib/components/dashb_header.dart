import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pasa_admin/bloc/auth/auth_bloc.dart';
import 'package:rtdb_repo/models/admin.dart';

import '../bloc/login/credential_cubit.dart';
import '../constants/constants.dart';
import 'dashb_label.dart';



class DashbHeader extends StatelessWidget {

  final AdminType adminType;
  final String? name;

  const DashbHeader({
    required this.adminType,
    this.name,
    super.key
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 2,
          child: DashbLabel(title: AdminTypeReadable(adminType).adminTypeStr)
        ),
        Expanded(
            flex: 4,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SizedBox(
                  height: 50,
                  width: 50,
                  child: ClipOval(
                    child: Image.asset(Constant().pasaIcon),
                  ),
                ),
                const SizedBox(width: 20),
                Text(
                  name == null ? AdminTypeReadable(adminType).adminTypeStr : '$name${adminType == AdminType.nurse ? ', RN' : ''}',
                  style: const TextStyle(
                    fontSize: 18
                  ),
                ),
                const SizedBox(width: 20),
                DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    icon: const Icon(
                      Icons.arrow_drop_down,
                      color: Colors.black,
                    ),
                    items: const [
                      DropdownMenuItem<String>(
                        value: 'Logout',
                        child: Text('Logout'),
                      ),
                    ],
                    onChanged: (value) {
                      switch (value) {
                        case 'Logout':
                          context.read<CredentialCubit>().resetAdminType();
                          context.read<AuthBloc>().add(const AuthLogoutRequestedEvent());
                          break;
                      }
                    },
                  ),
                ),
              ],
            )
            )
            
      ],
    );
  }
}
