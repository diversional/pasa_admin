import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rtdb_repo/models/patient.dart';
import 'package:rtdb_repo/models/pending_room_alert.dart';

import '../bloc/rtdb/rtdb_cubit.dart';

class DialogEmergency extends StatelessWidget {

  final List<PendingRoomAlert> pendingRoomAlerts;
  final Function() onConfirm;

  DialogEmergency({
    required this.pendingRoomAlerts,
    required this.onConfirm,
    super.key
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.zero,
      ),
      backgroundColor: const Color(0xffFF2C2C),
      child: SizedBox(
        width: MediaQuery.of(context).size.width * 0.4,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                decoration: const BoxDecoration(
                  color: Color(0xffFF2C2C),
                ),
                child: Row(
                  children: [
                    const Expanded(
                      child: Center(
                        child: Text(
                          '! ! ! EMERGENCY ! ! !',
                          style: TextStyle(
                              fontSize: 24, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    IconButton(
                      icon: const Icon(Icons.close),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                ),
              ),
            ),
            Container(
              decoration: const BoxDecoration(
                color: Color(0xffF4B1B1),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Patient\'s Information',
                            style: TextStyle(fontSize: 18),
                          ),
                          const SizedBox(height: 10.0),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: _supplyText(),
                          )
                        ],
                      ),
                    ),
                  ),
                  Flexible(
                    fit: FlexFit.tight,
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          (pendingRoomAlerts.length == 1) ?
                          Text(
                            'ROOM: ${pendingRoomAlerts.first.roomNumber}',
                            style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                          ) : const SizedBox(),
                          const SizedBox(height: 10),
                          Wrap(
                            children: _supplyConfirmButton(context),
                          ), // iterate button
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  List<Widget> _supplyText() {
    return pendingRoomAlerts.map((entry) {
      if (entry.roomAlertType == 1) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Divider(
              color: Colors.black,
              height: 2,
              thickness: 1,
            ),
            const SizedBox(height: 4.0),
            Text('Name: ${entry.name}'),
            Text('Room: ${entry.roomNumber}'),
            Text('Bed: ${entry.bedNumber}'),
            const SizedBox(height: 4.0),
          ],
        );
      } else {
        return const SizedBox();
      }
    }).toList();
  }

  List<Widget> _supplyConfirmButton(BuildContext context) {
    final singleAlert = pendingRoomAlerts.length == 1;

    return pendingRoomAlerts.map((entry) {
      final patientId = entry.patientId;

      if (entry.roomAlertType == 1) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 10.0),
            MaterialButton(
              shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
              onPressed: () {
                final matchedAlertOfPatient = pendingRoomAlerts.where(
                    (element) => element.patientId == patientId
                ).first;
                context.read<RealtimeDatabaseCubit>().writeHistoryPendingRoomAlert(matchedAlertOfPatient);
                context.read<RealtimeDatabaseCubit>().deleteRoomAlert(patientId);
                Navigator.of(context).pop();
                onConfirm();
              },
              color: const Color(0xff091B2A),
              child: Text(
                singleAlert
                    ? 'CONFIRM PATIENT'
                    : 'CONFIRM PATIENT: ${entry.name}',
                style: const TextStyle(color: Colors.white),
              ),
            )
          ],
        );
      } else {
        return const SizedBox();
      }

    }).toList();
  }

}
