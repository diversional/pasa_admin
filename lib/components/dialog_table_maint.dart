
import 'package:flutter/material.dart';

import 'package:rtdb_repo/models/admin.dart';

class DialogTableMaint extends StatelessWidget {

  final String roomNumber, name, reason;
  final AdminType adminType;

  const DialogTableMaint({
    required this.roomNumber,
    required this.name,
    required this.adminType,
    required this.reason,
    super.key
  });


    @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.zero,
      ),
      backgroundColor: const Color(0xffA6A6A6),
      child: SizedBox(
        width: MediaQuery.of(context).size.width * 0.4,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                decoration: const BoxDecoration(
                  color: Color(0xffA6A6A6),
                ),
                child: Row(
                  children: [
                    Expanded(
                      child: Center(
                        child: Text(
                          AdminTypeReadable(adminType).adminTypeStr,
                          style: const TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                    ),
                    IconButton(
                      icon: const Icon(Icons.close),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                ),
              ),
            ),
            Container(
              decoration: const BoxDecoration(
                color: Color(0xffE5E5E5),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Room #: $roomNumber'),
                          Text('Name #: $name'),

                          const SizedBox(height: 10),
                          Text(
                            AdminTypeReadable(adminType).adminTypeStr,
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text('Reason: $reason')
                        ],
                      ),
                    ),
                  ),
                  Flexible(
                    fit: FlexFit.tight,
                    flex: 1,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          const SizedBox(height: 10),
                          const SizedBox(
                            height: 70,
                            child: TextField(
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white,
                                border: OutlineInputBorder(),
                                contentPadding: EdgeInsets.symmetric(vertical: 40)
                              ),
                            ),
                          ),
                          const SizedBox(height: 10),
                          Row(
                            children: [
                              MaterialButton(
                                shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                color:const Color(0xff091B2A),
                                child: const Text(
                                  'CLEAR ALL',
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                              const SizedBox(width: 10),
                              MaterialButton(
                                shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                color:const Color(0xff091B2A),
                                child: const Text(
                                  'SEND',
                                  style: TextStyle(color: Colors.white),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }



}