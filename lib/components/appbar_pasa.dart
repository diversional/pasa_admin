import 'package:flutter/material.dart';
import 'package:pasa_admin/constants/constants.dart';

class AppbarPASA extends StatelessWidget with PreferredSizeWidget {

  const AppbarPASA({super.key});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      title: Text(
        'PASA',
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 20.0,
          color: Constant().pasaYellowColor,
        ),
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);

}
