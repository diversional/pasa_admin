import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:pasa_admin/bloc/rtdb/rtdb_cubit.dart';
import 'package:pasa_admin/components/button_rounded_yellow.dart';
import 'package:pasa_admin/components/textfield_rounded.dart';
import 'package:rtdb_repo/models/patient.dart';

class DialogAddNewPatient extends StatelessWidget {

  final Patient? toUpdatePatient;

  final nameController = TextEditingController();
  final ageController = TextEditingController();
  final genderController = TextEditingController();
  final civilStatusController = TextEditingController();
  final religionController = TextEditingController();
  final dateOfAdmissionController = TextEditingController();
  final chiefComplaintController = TextEditingController();
  final roomNumberController = TextEditingController();
  final bedController = TextEditingController();
  final tempPassController = TextEditingController();

  DialogAddNewPatient({this.toUpdatePatient, super.key});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.zero,
      ),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SizedBox(
          width: MediaQuery.of(context).size.width * 0.7,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                        const SizedBox(height: 10),
                        TextFieldRounded(
                          textStart: 'Name',
                          nameType: true,
                          bgColor: const Color(0xffB7DBC3),
                          textController: nameController..text = toUpdatePatient?.name ?? '',
                        ),
                        const SizedBox(height: 10),
                        TextFieldRounded(
                          numOnly: true,
                          textStart: 'Age',
                          bgColor: const Color(0xffB7DBC3),
                          textController: ageController..text = '${toUpdatePatient?.age ?? ''}',
                        ),
                        const SizedBox(height: 10),
                        TextFieldRounded(
                          textStart: 'Gender',
                          capitalOneWord: true,
                          bgColor: const Color(0xffB7DBC3),
                          textController: genderController..text = toUpdatePatient?.gender ?? '',
                        ),
                        const SizedBox(height: 10),
                        TextFieldRounded(
                          textStart: 'Civil Status',
                          capitalOneWord: true,
                          bgColor: const Color(0xffB7DBC3),
                          textController: civilStatusController..text = toUpdatePatient?.civilStatus ?? '',
                        ),
                        const SizedBox(height: 10),
                        TextFieldRounded(
                          textStart: 'Religion',
                          nameType: true,
                          bgColor: const Color(0xffB7DBC3),
                          textController: religionController..text = toUpdatePatient?.religion ?? '',
                        ),
                        const SizedBox(height: 10),
                        TextFieldRounded(
                          textStart: 'Date of Admission',
                          bgColor: const Color(0xffB7DBC3),
                          textController: dateOfAdmissionController..text = toUpdatePatient?.dateOfAdmission ?? '',
                        ),
                      ],
                    ),
                  )),
              const SizedBox(width: 10),
              Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        const SizedBox(height: 10),
                        TextFieldRounded(
                          textStart: 'Chief Complaint (optional)',
                          bgColor: const Color(0xffB7DBC3),
                          expanded: true,
                          textController: chiefComplaintController..text = toUpdatePatient?.chiefComplaint ?? '',
                        ),
                        const SizedBox(height: 10),
                        TextFieldRounded(
                          textStart: 'Temporary Password',
                          bgColor: const Color(0xffB7DBC3),
                          textController: tempPassController..text = toUpdatePatient?.tempPass ?? '',
                        ),
                        const SizedBox(height: 10),
                        TextFieldRounded(
                          enabled: toUpdatePatient == null,
                          numOnly: true,
                          textStart: 'Bed',
                          bgColor: const Color(0xffB7DBC3),
                          textController: bedController..text = '${toUpdatePatient?.bedNumber ?? ''}',
                        ),
                        const SizedBox(height: 10),
                        TypeAheadField(
                          textFieldConfiguration: TextFieldConfiguration(
                              keyboardType: TextInputType.number,
                              controller: roomNumberController..text = toUpdatePatient?.roomNumber ?? '',
                              decoration: InputDecoration(
                                hintText: 'Room #',
                                filled: true,
                                fillColor: const Color(0xffB7DBC3),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(25.0),
                                ),
                                contentPadding: const EdgeInsets.symmetric(horizontal: 20.0),
                              )
                          ),
                          suggestionsCallback: (pattern) async {
                            final rooms = context.read<RealtimeDatabaseCubit>()
                                .state.roomList;

                            if (rooms == null) {
                              return [];
                            }

                            List<String> roomNumbers = rooms.where((room) {
                              return room.number!.toLowerCase().contains(pattern.toLowerCase());
                            }).toList()
                                .map((room) => room.number!).toList();

                            return roomNumbers;
                          },
                          itemBuilder: (context, suggestion) {
                            return ListTile(
                              title: Text(suggestion),
                            );
                          },
                          onSuggestionSelected: (suggestion) {
                            roomNumberController.text = suggestion;
                          },
                        ),
                        const SizedBox(height: 10),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            ButtonRoundedYellow(
                              text: 'CLOSE',
                              pressListener: () => Navigator.of(context).pop(),
                            ),
                            const SizedBox(width: 20.0),
                            ButtonRoundedYellow(
                              text: 'CLEAR ALL',
                              pressListener: () => _clearTextEditingControllers(),
                            ),
                            const SizedBox(width: 10.0),
                            ButtonRoundedYellow(
                              text: 'UPDATES',
                              pressListener: () {
                                _createNewPatient(context);
                              },
                            )
                          ]
                        )
                    ],
                  ),
                )
              ),
            ],
          ),
        ),
      ),
    );
  }

  _createNewPatient(BuildContext context) {
    final name = nameController.text;
    final age = int.tryParse(ageController.text);
    final gender = genderController.text;
    final civilStatus = civilStatusController.text;
    final religion = religionController.text;
    final dateOfAdmission = dateOfAdmissionController.text;
    final chiefComplaint = chiefComplaintController.text;
    final tempPass = tempPassController.text;
    final roomNumber = roomNumberController.text;
    final bedNumber = int.tryParse(bedController.text);

    if (name.isEmpty ||
        (age == null || age == 0) ||
        gender.isEmpty ||
        civilStatus.isEmpty ||
        religion.isEmpty ||
        dateOfAdmission.isEmpty ||
        tempPass.isEmpty ||
        roomNumber.isEmpty ||
        (bedNumber == null || bedNumber == 0)) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Please fill in the necessary fields')),
      );
      return;
    }

    final timeStampId = DateTime.now().millisecondsSinceEpoch.toString();

    final patientToWrite =
      toUpdatePatient == null ?
          Patient(
            id: timeStampId,
            authId: timeStampId,
            roomNumber: roomNumber,
            name: name,
            gender: gender,
            bedNumber: bedNumber,
            age: age,
            civilStatus: civilStatus,
            religion: religion,
            dateOfAdmission: dateOfAdmission,
            chiefComplaint: chiefComplaint,
            tempPass: tempPass,
          )
          :
          toUpdatePatient!.copyWith(
            name: name,
            age: age,
            gender: gender,
            civilStatus: civilStatus,
            religion: religion,
            dateOfAdmission: dateOfAdmission,
            chiefComplaint: chiefComplaint,
            tempPass: tempPass,
            roomNumber: roomNumber,
            bedNumber: bedNumber,
          );

    final roomInfo = context.read<RealtimeDatabaseCubit>().state.roomList
      ?.where((queriedRoom) => queriedRoom.number == roomNumber).first;

    if (roomInfo != null && bedNumber > roomInfo.bedCount!) {
      ScaffoldMessenger.of(context)
          .showSnackBar(
          SnackBar(content: Text(
              'Room $roomNumber has a maximum of ${roomInfo.bedCount} bed(s)'
          ))
      );
      return;
    }

    final patientList = context.read<RealtimeDatabaseCubit>().state.patientList;

    if (patientList == null || patientList.isEmpty) {
      _writePatient(context, timeStampId, patientToWrite);
      return;
    }

    final matchingPatients = patientList.where((patient) =>
        patient.roomNumber == patientToWrite.roomNumber &&
        patient.bedNumber == patientToWrite.bedNumber).toList();

    if (matchingPatients.isNotEmpty && toUpdatePatient == null) {
      ScaffoldMessenger.of(context)
        .showSnackBar(
          SnackBar(content: Text('Bed $bedNumber is occupied'))
        );
      return;
    }
    _writePatient(context, timeStampId, patientToWrite);
  }

  _writePatient(
      BuildContext context,
      String timeStampId,
      Patient patient
      ) {
    context.read<RealtimeDatabaseCubit>().createNewPatient(
      patient,
      (toUpdatePatient != null) ? (toUpdatePatient!.authId ?? toUpdatePatient!.id)! : timeStampId,
      update: toUpdatePatient != null,
    );

    _clearTextEditingControllers();
    Navigator.of(context).pop();
  }

  _clearTextEditingControllers() {
    nameController.clear();
    ageController.clear();
    genderController.clear();
    civilStatusController.clear();
    religionController.clear();
    dateOfAdmissionController.clear();
    chiefComplaintController.clear();
    roomNumberController.clear();
    bedController.clear();
  }

}