import 'package:audioplayers/audioplayers.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pasa_admin/components/dialog_details.dart';
import 'package:rtdb_repo/models/chat_message.dart';
import 'package:rtdb_repo/models/patient.dart';
import 'package:rtdb_repo/models/request_maintenance.dart';

import 'package:intl/intl.dart' as date_format;

import '../bloc/rtdb/rtdb_cubit.dart';
import '../constants/constants.dart';
import '../cookie_manager.dart';
import '../models/chat_cookie.dart';
import '../models/maintenance_cookie.dart';
import '../pages/chat_page.dart';
import 'dashb_component_table_listview.dart';
import 'dashb_component_table_property.dart';
import 'dialog_delete_confirmation.dart';
import 'popup_menu_option.dart';

class DashbMaintHousekeeping extends StatelessWidget {

  final List<RequestMaintenance> housekeepingMaintenanceRequestList;
  final List<ChatMessage>? observeChatMessage;
  final Function(Patient) onTap;
  final Function(Patient)? onNewMessage;

  DashbMaintHousekeeping({
    required this.housekeepingMaintenanceRequestList,
    this.observeChatMessage,
    required this.onTap,
    required this.onNewMessage,
    super.key
  }) {
    housekeepingMaintenanceRequestList.sort((a, b) {
      int aTime = int.parse(a.timeStampAsId ?? '0');
      int bTime = int.parse(b.timeStampAsId ?? '0');
      return bTime.compareTo(aTime);
    });
  }

  @override
  Widget build(BuildContext context) {

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text(
          'Requests for Housekeeping',
          style: TextStyle(fontSize: 30),
        ),
        const SizedBox(height: 10),
        const DashbComponentTableProperty(
          properties: ['Room #', 'Message', 'Time', 'More'],
        ),
        DashbComponentTableListView(
          listLength: housekeepingMaintenanceRequestList.length,
          children: (index) {
            final maintenanceReq = housekeepingMaintenanceRequestList[index];


            final patientMessageList = observeChatMessage?.where((chatMessage) {
              return chatMessage.senderAssignedId == maintenanceReq.patientId;
            }).toList();

            return NotifiableMaintenanceItem(
                requestMaintenance: maintenanceReq,
                filteredMessages: patientMessageList ?? [],
                onTap: onTap,
                onNewMessage: onNewMessage
            );
          },
          onTapListener: (index) {
            
          },
        ),
      ],
    );
  }

}

class NotifiableMaintenanceItem extends StatelessWidget {

  final audioPlayer = AudioPlayer();

  late int chatCookiedCount;

  final RequestMaintenance requestMaintenance;
  final List<ChatMessage> filteredMessages;
  final Function(Patient) onTap;
  final Function(Patient)? onNewMessage;

  NotifiableMaintenanceItem({
    required this.requestMaintenance,
    required this.filteredMessages,
    required this.onTap,
    required this.onNewMessage,
    Key? key
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final patient = context.read<RealtimeDatabaseCubit>()
        .state.patientList?.where(
            (patient) => patient.id == requestMaintenance.patientId
    ).first;
    final roomNumber = patient?.roomNumber;

    final time =
    date_format.DateFormat(
        'yyyy-MM-dd hh:mm:ss a'
    ).format(
        DateTime.fromMillisecondsSinceEpoch(
            int.parse(requestMaintenance.timeStampAsId!)
        )
    );

    final patientId = patient!.id;

    final filteredChatMessageLength = filteredMessages.length;

    // Cache
    final adminAuthId = FirebaseAuth.instance.currentUser!.uid;
    final chatCookieKey = '$patientId$adminAuthId';

    final chatCookieStr = CookieManager.getCookie(chatCookieKey);

    if (chatCookieStr.isEmpty) {
      CookieManager.addToCookie(
          chatCookieKey,
          ChatCookie(
              keyAndSenderPatientId: chatCookieKey,
              messageLength: 0
          ).toStringJson()
      );
      chatCookiedCount = 0;
    } else {
      chatCookiedCount = ChatCookie.fromJson(
          ChatCookie.toMapJson(chatCookieStr)
      ).messageLength ?? 0;

      if (chatCookiedCount < filteredChatMessageLength) {
        CookieManager.addToCookie(
            chatCookieKey,
            ChatCookie(
                keyAndSenderPatientId: chatCookieKey,
                messageLength: filteredChatMessageLength
            ).toStringJson()
        );
        _playRadioButtonTone();

        if (onNewMessage != null) {
          onNewMessage!(patient);
        }
      }
    }
    // Cache

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(roomNumber ?? requestMaintenance.patientId ?? ''),
          Text(requestMaintenance.message ?? ''),
          Text(
              date_format.DateFormat(
                  'MM/dd/yyyy hh:mm a'
              ).format(
                  DateTime.fromMillisecondsSinceEpoch(
                      int.parse(requestMaintenance.timeStampAsId!)
                  )
              )
          ),
          PopupMenuOption(
            onDetailsTap: () {
              showDialog(
                  context: context,
                  builder: (context) => DialogDetails(
                    children: [
                      Text('Room #: $roomNumber'),
                      const SizedBox(height: 10.0),
                      Text("Message: '${requestMaintenance.message}'"),
                      const SizedBox(height: 10.0),
                      Text("Time Requested: $time"),
                    ],
                  )
              );
            },
            onChatTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) {
                  return ChatPage(
                    patientId: requestMaintenance.patientId!,
                    notifToken: patient.notifToken,
                  );
                }),
              );
            },
            onDeleteTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return DialogDeleteConfirmation(
                    name: '"${requestMaintenance.message}" request',
                  );
                },
              ).then((value) {
                if (value) {
                  context.read<RealtimeDatabaseCubit>()
                      .deleteHousekeepingMaintenanceRequest(requestMaintenance);

                  final houseMaintCountStr = CookieManager.getCookie(CookieKeys.houseMaintKey);

                  if (houseMaintCountStr.isNotEmpty) {
                    final cookieTechMaintCount = MaintenanceCookie.fromJson(
                        MaintenanceCookie.toMapJson(houseMaintCountStr)
                    ).housekeepingRequestsCount ?? 0;
                    CookieManager.addToCookie(
                        CookieKeys.houseMaintKey,
                        MaintenanceCookie(
                            housekeepingRequestsCount: cookieTechMaintCount - 1
                        ).toStringJson()
                    );
                  }

                  ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('Housekeeping request successfully deleted'))
                  );
                }
              });
            },

            onClearChat: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return const DialogDeleteConfirmation(
                    name: 'the conversation',
                  );
                },
              ).then((value) {
                if (value) {
                  final adminUid = FirebaseAuth.instance.currentUser?.uid ?? '';

                  context.read<RealtimeDatabaseCubit>()
                      .deleteAdminAndPatientConvo(adminUid, requestMaintenance.patientId!);
                  ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('Conversation cleared'))
                  );
                }
              });
            },

          )
        ],
      ),
    );
  }

  _playRadioButtonTone() async {
    await audioPlayer.play(UrlSource(Constant().radioButtonTone));
  }

}
