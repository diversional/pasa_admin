import 'package:flutter/material.dart';

class DialogDeleteConfirmation extends StatelessWidget {
  final String name;

  const DialogDeleteConfirmation({required this.name});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Delete Confirmation'),
      content: Text('Are you sure you want to delete $name?'),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.of(context).pop(false),
          child: const Text('CANCEL'),
        ),
        ElevatedButton(
          onPressed: () => Navigator.of(context).pop(true),
          child: const Text('DELETE'),
        ),
      ],
    );
  }

}
