import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextFieldRounded extends StatelessWidget {
  final String textStart;
  final TextEditingController textController;
  final Color bgColor;
  final bool? expanded, enabled, numOnly, nameType, capitalOneWord;

  TextFieldRounded({
    required this.textStart,
    required this.textController,
    required this.bgColor,
    this.nameType,
    this.capitalOneWord,
    this.expanded,
    this.enabled,
    this.numOnly,
    super.key
  });

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Flexible(
                flex: 1,
                child: Text(
                  textStart,
                  style: const TextStyle(fontSize: 16.0),
                ),
              ),
            ),
            const SizedBox(width: 5.0),
            Flexible(
              flex: 3,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: bgColor,
                ),
                child: (expanded != null && expanded!)
                    ? SizedBox(
                        height: 70,
                        child: TextField(
                          controller: textController,
                          decoration: const InputDecoration(
                              border: InputBorder.none,
                              contentPadding:
                                  EdgeInsets.symmetric(vertical: 40)),
                        ),
                      )
                    :
            (nameType != null && nameType!) ?
                TextField(
                  controller: textController,
                  decoration: const InputDecoration(
                    border: InputBorder.none,
                    contentPadding:
                    EdgeInsets.symmetric(horizontal: 20.0),
                  ),
                  textInputAction: TextInputAction.next,
                  textCapitalization: TextCapitalization.words,
                  onChanged: (value) {
                    final text = value
                        .toLowerCase()
                        .split(' ')
                        .map((word) =>
                    word.length > 1
                        ? '${word[0].toUpperCase()}${word.substring(1)}'
                        : word.toUpperCase())
                        .join(' ');
                    textController.value = TextEditingValue(
                      text: text,
                      selection: TextSelection.collapsed(offset: text.length),
                    );
                  },
                )
                :
            (capitalOneWord != null && capitalOneWord!) ?
                TextField(
                  controller: textController,
                  decoration: const InputDecoration(
                    border: InputBorder.none,
                    contentPadding:
                    EdgeInsets.symmetric(horizontal: 20.0),
                  ),
                  textInputAction: TextInputAction.next,
                  textCapitalization: TextCapitalization.words,
                  onChanged: (value) {
                    final text = value
                        .trim()
                        .toLowerCase()
                        .split(' ')
                        .map((word) =>
                    word.length > 1
                        ? '${word[0].toUpperCase()}${word.substring(1)}'
                        : word.toUpperCase())
                        .join(' ');
                    textController.value = TextEditingValue(
                      text: text,
                      selection: TextSelection.collapsed(offset: text.length),
                    );
                  },
                )
                :
                TextField(
                        keyboardType: (numOnly != null && numOnly!)
                            ? TextInputType.number
                            : TextInputType.text,
                        inputFormatters: (numOnly != null && numOnly!)
                            ? [FilteringTextInputFormatter.digitsOnly]
                            : null,
                        enabled: enabled ?? true,
                        controller: textController,
                        decoration: const InputDecoration(
                          border: InputBorder.none,
                          contentPadding:
                              EdgeInsets.symmetric(horizontal: 20.0),
                        ),
                      ),
              ),
            )
          ],
        )
      ],
    );
  }
}
