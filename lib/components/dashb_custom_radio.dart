import 'package:flutter/material.dart';

class DashbCustomRadio extends StatelessWidget {

  final String title;
  final int groupValue;
  final int value;
  final Function() onTap;
  final bool showNotification;

  const DashbCustomRadio({
    required this.title,
    required this.groupValue,
    required this.value,
    required this.onTap,
    this.showNotification = false,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(20),
          clipBehavior: Clip.antiAlias,
          child: Container(
            width: 200,
            color: groupValue == value
                ? const Color.fromARGB(255, 117, 172, 216)
                : const Color(0xFFc0e3ff),
            child: InkWell(
              onTap: onTap,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  margin: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Text(
                      title,
                      style: const TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        if (showNotification)
          Positioned(
            top: 10,
            right: 10,
            child: Container(
              width: 10,
              height: 10,
              decoration: const BoxDecoration(
                color: Color(0xFFF5A901),
                shape: BoxShape.circle,
              ),
            ),
          ),
      ],
    );
  }

}
