import 'package:flutter/material.dart';

import '../constants/constants.dart';

class ButtonRoundedYellow extends StatelessWidget {

  final String text;
  final Function() pressListener;

  const ButtonRoundedYellow({
    required this.text,
    required this.pressListener,
    super.key
  });

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      color: Constant().pasaYellowColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0)
      ),
      onPressed: () => pressListener(),
      child: Text(text)
    );
  }

}