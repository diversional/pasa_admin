import 'package:flutter/material.dart';

class DashbComponentTableProperty extends StatelessWidget {

  final List<String> properties;

  const DashbComponentTableProperty({
    required this.properties,
    super.key
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.grey[300]!,
          width: 1.0,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: properties.map((value) {
            return Text(value);
          }).toList(),
        ),
      ),
    );
  }
}
