import 'package:audioplayers/audioplayers.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pasa_admin/bloc/auth/auth_bloc.dart';
import 'package:pasa_admin/bloc/rtdb/rtdb_cubit.dart';

import 'package:pasa_admin/components/dashb_maint_fragment_housekeeping.dart';
import 'package:pasa_admin/components/dashb_maint_fragment_technician.dart';
import 'package:pasa_admin/components/dashb_nurse_fragment_pending_requests.dart';
import 'package:pasa_admin/components/dashb_nurse_fragment_rooms.dart';
import 'package:pasa_admin/cookie_manager.dart';
import 'package:pasa_admin/models/maintenance_cookie.dart';
import 'package:rtdb_repo/models/admin.dart';
import 'package:rtdb_repo/models/chat_message.dart';

import '../components/button_rounded_yellow.dart';
import '../components/dashb_header.dart';
import '../components/dashb_custom_radio.dart';

import '../components/dashb_nurse_fragment_patients.dart';
import '../components/dialog_add_new_patient.dart';
import '../components/dialog_add_new_room.dart';
import '../constants/constants.dart';
import '../models/maintenance_cookie.dart';
import 'chat_page.dart';

class DashbPage extends StatefulWidget {

  final scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

  bool playedToneFlag1 = false, playedToneFlag2 = false;

  static Page<void> page() => MaterialPage(
    child: DashbPage()
  );

  DashbPage({
    super.key
  });

  @override
  State<DashbPage> createState() => _DashbPageState();
}

class _DashbPageState extends State<DashbPage> {

  final audioPlayer = AudioPlayer();

  late Admin _admin;
  late AdminType _adminType;
  int _fragmentIndexGroupValue = 0;

  bool _tappedFlag1 = false, _tappedFlag2 = false;

  late int cookieTechReqCount, cookieHouseReqCount;

  @override
  void initState() {
    final deserAdmin = CookieManager.getCookie(CookieKeys.adminKey);

    if (deserAdmin.isEmpty) {
      context.read<AuthBloc>().add(const AuthLogoutRequestedEvent());
      return;
    }

    _admin = Admin.fromJson(Admin.toMapJson(deserAdmin));
    _adminType = AdminTypeReadable.serialize(_admin.adminType!);

    final techMaintCountStr = CookieManager.getCookie(CookieKeys.techMaintKey);
    final houseMaintCountStr = CookieManager.getCookie(CookieKeys.houseMaintKey);

    if (techMaintCountStr.isEmpty) {
      cookieTechReqCount = 0;
    } else {
      cookieTechReqCount = MaintenanceCookie.fromJson(
          MaintenanceCookie.toMapJson(techMaintCountStr)
      ).techRequestsCount ?? 0;
    }

    if (houseMaintCountStr.isEmpty) {
      cookieHouseReqCount = 0;
    } else {
      cookieHouseReqCount = MaintenanceCookie.fromJson(
          MaintenanceCookie.toMapJson(houseMaintCountStr)
      ).housekeepingRequestsCount ?? 0;
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScaffoldMessenger(
      key: widget.scaffoldMessengerKey,
      child: Scaffold(
        backgroundColor: const Color.fromARGB(255, 230, 230, 230),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              DashbHeader(
                adminType: _adminType,
                name: _admin.name,
              ),
              const SizedBox(height: 20),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(height: 10),
                          BlocBuilder<RealtimeDatabaseCubit, RealtimeDatabaseState>(
                            buildWhen: (prev, curr) {
                              switch (_adminType) {
                                case AdminType.nurse:
                                  return prev.pendingRoomAlerts != curr.pendingRoomAlerts;
                                case AdminType.maintenance:
                                  return prev.technicianMaintenanceRequestList != curr.technicianMaintenanceRequestList;
                              }
                              return false;
                            },
                            builder: (context, state) {

                              final hasAlert =
                              _adminType == AdminType.nurse ?
                                state.pendingRoomAlerts?.isNotEmpty ?? false
                                  :
                                state.technicianMaintenanceRequestList?.isNotEmpty ?? false;

                              switch (_adminType) {
                                case AdminType.nurse:
                                  if (hasAlert && !widget.playedToneFlag1) {
                                    _playRadioButtonTone();
                                    widget.playedToneFlag1 = true;
                                  }

                                  if (!hasAlert) {
                                    _tappedFlag1 = false;
                                  }
                                  break;
                                case AdminType.maintenance:
                                  if (cookieTechReqCount < (state.technicianMaintenanceRequestList?.length ?? 0)) {
                                    _playRadioButtonTone();
                                    _tappedFlag1 = false;
                                  }
                                  break;
                              }


                              return DashbCustomRadio(
                                  title: _getFragmentTitleByModeAndIndex(_adminType, 0),
                                  groupValue: _fragmentIndexGroupValue,
                                  value: 0,
                                  showNotification:
                                    _adminType == AdminType.nurse ?
                                    hasAlert && !_tappedFlag1 :
                                    (!_tappedFlag1 || (cookieTechReqCount < (state.technicianMaintenanceRequestList?.length ?? 0))) ,
                                  onTap: () {
                                    setState(() {
                                      _fragmentIndexGroupValue = 0;
                                      _tappedFlag1 = true;
                                    });
                                  }
                              );
                            },
                          ),
                          const SizedBox(height: 20),

                          _adminType == AdminType.nurse ?
                          Column(
                            children: [
                              BlocBuilder<RealtimeDatabaseCubit, RealtimeDatabaseState>(
                                buildWhen: (prev, curr) {
                                  return prev.messageChange?.length != curr.messageChange?.length;
                                },
                                builder: (context, state) {
                                  final hasAlert = state.messageChange?.isNotEmpty ?? false;

                                  if (hasAlert && !widget.playedToneFlag2) {
                                    widget.playedToneFlag2 = true;
                                  }

                                  if (!hasAlert) {
                                    _tappedFlag2 = false;
                                  }

                                  return DashbCustomRadio(
                                      title: _getFragmentTitleByModeAndIndex(_adminType, 1),
                                      groupValue: _fragmentIndexGroupValue,
                                      value: 1,
                                      showNotification: hasAlert && !_tappedFlag2,
                                      onTap: () {
                                        setState(() => _fragmentIndexGroupValue = 1);
                                      }
                                  );
                                },
                              ),
                              const SizedBox(height: 20),
                              DashbCustomRadio(
                                  title: _getFragmentTitleByModeAndIndex(_adminType, 2),
                                  groupValue: _fragmentIndexGroupValue,
                                  value: 2,
                                  showNotification: false,
                                  onTap: () {
                                    setState(() => _fragmentIndexGroupValue = 2);
                                  }
                              )
                            ],
                          )
                          :
                          BlocBuilder<RealtimeDatabaseCubit, RealtimeDatabaseState>(
                            buildWhen: (prev, curr) {
                              return prev.housekeepingMaintenanceRequestList?.length
                                  != curr.housekeepingMaintenanceRequestList?.length;
                            },
                            builder: (context, state) {
                              if (cookieHouseReqCount < (state.housekeepingMaintenanceRequestList?.length ?? 0)) {
                                _playRadioButtonTone();
                                _tappedFlag2 = false;
                              }

                              return DashbCustomRadio( // Maintenance Housekeeping
                                  title: _getFragmentTitleByModeAndIndex(
                                      _adminType,
                                      _adminType == AdminType.nurse ? 2 : 1
                                  ),

                                  groupValue: _fragmentIndexGroupValue,
                                  value: _adminType == AdminType.nurse ? 2 : 1,
                                  showNotification: _adminType == AdminType.nurse ?
                                      false
                                      :
                                      (!_tappedFlag2 && (cookieHouseReqCount < (state.housekeepingMaintenanceRequestList?.length ?? 0))),
                                  onTap: () {
                                    setState(() {
                                      _fragmentIndexGroupValue = _adminType == AdminType.nurse ? 2 : 1;
                                      cookieHouseReqCount = (state.housekeepingMaintenanceRequestList?.length ?? 0);
                                      _tappedFlag2 = true;
                                    });
                                  }
                              );
                            },
                          )
                        ],
                      ),
                    ),
                    Expanded( // Fragment
                      flex: 2,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10)
                        ),
                        height: MediaQuery.of(context).size.height * 0.6,
                        width: MediaQuery.of(context).size.width,
                        child: Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: _currentFragment(
                            _fragmentIndexGroupValue
                          ),
                        ),
                      )
                    )
                  ],
                )
            ],
          ),
        ),
      ),
    );
  }

  String _getFragmentTitleByModeAndIndex(AdminType adminType, int index) {
    switch (adminType) {
      case AdminType.nurse:
        return Constant().nurseFragmentTitles[index];
      case AdminType.maintenance:
        return Constant().maintenanceFragmentTitles[index];
      default: return '';
    }
  }

  Widget _currentFragment(int index) {

    switch (_adminType) {
      case AdminType.nurse:
        switch (index) {
          case 0:
            context.read<RealtimeDatabaseCubit>().listenPendingRoomAlerts();
            context.read<RealtimeDatabaseCubit>().listenAllRoom();

            return BlocBuilder<RealtimeDatabaseCubit, RealtimeDatabaseState>(
                buildWhen: (prev, curr) {
                  return
                  (prev.pendingRoomAlerts?.length != curr.pendingRoomAlerts?.length)
                  ||
                  (prev.roomList?.length != curr.roomList?.length);
                },
                builder: (context, state) {
                  final roomsList = state.roomList;

                  return roomsList == null || roomsList.isEmpty ?
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text('No existing rooms to show'),
                        const SizedBox(height: 10.0),
                        ButtonRoundedYellow(
                          text: 'ADD NEW ROOM',
                          pressListener: () {
                            showDialog(
                              context: context,
                              builder: (context) => DialogAddNewRoom()
                            );
                          },
                        )
                      ],
                    )
                    :
                    DashbNurseFragmentRoom(
                      roomsList: roomsList,
                      pendingRoomAlerts: state.pendingRoomAlerts,
                      onConfirm: () {
                        ScaffoldMessenger.of(context)
                            .showSnackBar(
                          SnackBar(
                            content: const Text(
                                'Room alert confirmed, the record has been moved to Pending Requests.'
                            ),
                            action: SnackBarAction(
                              label: 'VIEW',
                              onPressed: () {
                                setState(() {
                                  _fragmentIndexGroupValue = 2;
                                });
                              },
                            ),
                          )
                        );
                      },
                    );
                },
            );

          case 1:
            _tappedFlag2 = true;
            return BlocBuilder<RealtimeDatabaseCubit, RealtimeDatabaseState>(
              builder: (context, state) {
                final patientsList = state.patientList;

                if (patientsList == null || patientsList.isEmpty) {
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text('No patient to show'),
                      const SizedBox(height: 10.0),
                      ButtonRoundedYellow(
                        text: 'ADD NEW PATIENT',
                        pressListener: () {
                          showDialog(
                              context: context,
                              builder: (context) => DialogAddNewPatient()
                          );
                        },
                      )
                    ],
                  );
                }

                final adminAuthId = FirebaseAuth.instance.currentUser?.uid;

                if (adminAuthId != null) {
                  for (var patient in patientsList) {
                    context.read<RealtimeDatabaseCubit>().observeChatRoom(
                        adminAuthId, patient.id
                    );
                  }
                }

                patientsList.sort((aPatient, bPatient) {
                  return aPatient.roomNumber.compareTo(bPatient.roomNumber);
                });

                return DashbNurseFragmentPatients(
                  patientsList: patientsList,
                  observeChatMessage: state.messageChange,
                  onNewMessage: (patient) {
                    WidgetsBinding.instance.addPostFrameCallback((_) =>
                        widget.scaffoldMessengerKey.currentState?.showSnackBar(
                            _messageSnackbar(patient.name, patient.id)
                        )
                    );
                  },
                  onTap: (patient) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return ChatPage(
                          patientId: patient.id,
                        );
                      }),
                    );
                  },
                );
              },
            );

          case 2:

            return BlocBuilder<RealtimeDatabaseCubit, RealtimeDatabaseState>(
              builder: (context, state) {
                final pendingAlertsList = state.historyPendingRoomAlerts;
                pendingAlertsList?.sort((a, b) => b.timeStamp.compareTo(a.timeStamp));

                return
                pendingAlertsList == null || pendingAlertsList.isEmpty  ?
                const Center(child: Text('No pending alert to show'))
                :
                DashbNurseFragmentPendingRequests(pendingAlertsList: pendingAlertsList);
              }
            );
        }
        break;
      case AdminType.maintenance:

        context.read<RealtimeDatabaseCubit>().listenPatientsRoot();
        context.read<RealtimeDatabaseCubit>().listenTechMaintenanceRequests();
        context.read<RealtimeDatabaseCubit>().listenHousekeepingMaintenanceRequests();

        switch (index) {
          case 0:
            _tappedFlag1 = true;
            return BlocBuilder<RealtimeDatabaseCubit, RealtimeDatabaseState>(
              builder: (context, state) {
                final techMaintenanceRequestList = state.technicianMaintenanceRequestList;
                final listIsNullOrEmpty = techMaintenanceRequestList == null || techMaintenanceRequestList.isEmpty;

                final techCookieReqCountAsString = CookieManager.getCookie(CookieKeys.techMaintKey);

                if (techCookieReqCountAsString.isEmpty) {
                  if (!listIsNullOrEmpty) {
                    CookieManager.addToCookie(
                        CookieKeys.techMaintKey,
                        MaintenanceCookie(
                            techRequestsCount: techMaintenanceRequestList.length
                        ).toStringJson()
                    );
                  }
                  // else nothing to cache
                } else {
                  final newCount = techMaintenanceRequestList?.length ?? 0;


                  if (!listIsNullOrEmpty && cookieTechReqCount != newCount) {
                    CookieManager.addToCookie(
                        CookieKeys.techMaintKey,
                        MaintenanceCookie(techRequestsCount: newCount)
                            .toStringJson()
                    );

                    if (cookieTechReqCount < newCount) {
                      cookieTechReqCount = newCount;
                    }
                  }
                }

                final adminAuthId = FirebaseAuth.instance.currentUser?.uid;
                final patientsList = state.patientList;

                if (patientsList != null) {
                  if (adminAuthId != null) {
                    for (var patient in patientsList) {
                      context.read<RealtimeDatabaseCubit>().observeChatRoom(
                          adminAuthId, patient.id
                      );
                    }
                  }
                }

                return listIsNullOrEmpty ?
                  const Center(child: Text('No technician request to show'))
                  :
                  DashbMaintTechnician(
                      techMaintenanceRequestList: techMaintenanceRequestList,
                      observeChatMessage: state.messageChange,
                      onNewMessage: (patient) {
                        WidgetsBinding.instance.addPostFrameCallback((_) =>
                            widget.scaffoldMessengerKey.currentState?.showSnackBar(
                              _messageSnackbar(patient.name, patient.id)
                            )
                        );
                      },
                      onTap: (patient) {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) {
                            return ChatPage(
                              patientId: patient.id,
                            );
                          }),
                        );
                      },
                  );
              }
            );
          case 1:
            _tappedFlag2 = true;
            return BlocBuilder<RealtimeDatabaseCubit, RealtimeDatabaseState>(
                builder: (context, state) {
                  final housekeepingMaintenanceRequestList = state.housekeepingMaintenanceRequestList;
                  final listIsNullOrEmpty = housekeepingMaintenanceRequestList == null || housekeepingMaintenanceRequestList.isEmpty;

                  final houseCookieReqCountAsString = CookieManager.getCookie(CookieKeys.houseMaintKey);

                  if (houseCookieReqCountAsString.isEmpty) {
                    if (!listIsNullOrEmpty) {
                      CookieManager.addToCookie(
                          CookieKeys.houseMaintKey,
                          MaintenanceCookie(
                              housekeepingRequestsCount: housekeepingMaintenanceRequestList.length
                          ).toStringJson()
                      );
                    }
                    // else nothing to cache
                  } else {
                    final newCount = housekeepingMaintenanceRequestList?.length ?? 0;


                    if (!listIsNullOrEmpty) {
                      CookieManager.addToCookie(
                          CookieKeys.houseMaintKey,
                          MaintenanceCookie(housekeepingRequestsCount: newCount).toStringJson()
                      );

                      if (cookieHouseReqCount < newCount) {
                        _playRadioButtonTone();
                        cookieHouseReqCount = newCount;
                      }
                    }
                  }

                  final adminAuthId = FirebaseAuth.instance.currentUser?.uid;
                  final patientsList = state.patientList;

                  if (patientsList != null) {
                    if (adminAuthId != null) {
                      for (var patient in patientsList) {
                        context.read<RealtimeDatabaseCubit>().observeChatRoom(
                            adminAuthId, patient.id
                        );
                      }
                    }
                  }

                  return listIsNullOrEmpty ?
                    const Center(child: Text('No housekeeping request to show'))
                    :
                    DashbMaintHousekeeping(
                      housekeepingMaintenanceRequestList: housekeepingMaintenanceRequestList,
                      observeChatMessage: state.messageChange,
                      onNewMessage: (patient) {
                        WidgetsBinding.instance.addPostFrameCallback((_) =>
                            widget.scaffoldMessengerKey.currentState?.showSnackBar(
                              _messageSnackbar(patient.name, patient.id)
                            )
                        );
                      },
                      onTap: (patient) {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) {
                            return ChatPage(
                              patientId: patient.id,
                            );
                          }),
                        );
                      },
                    );
                }
            );
        }
      break;
    }

    throw Exception('Invalid admin type');
  }

  _playRadioButtonTone() async {
    await audioPlayer.play(UrlSource(Constant().radioButtonTone));
  }

  _messageSnackbar(patientName, patientId) {
    return SnackBar(
      duration: const Duration(seconds: 7),
      content: Text('New message from patient $patientName'),
      action: SnackBarAction(
        label: 'VIEW',
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) {
              return ChatPage(
                patientId: patientId,
              );
            }),
          );
        },
      ),
    );
  }



}
