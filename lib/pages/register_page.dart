import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:pasa_admin/bloc/auth/auth_bloc.dart';
import 'package:pasa_admin/bloc/rtdb/rtdb_cubit.dart';
import 'package:rtdb_repo/models/admin.dart';

import 'package:universal_html/html.dart' as html;

import '../bloc/login/credential_cubit.dart';
import '../constants/constants.dart';
import '../cookie_manager.dart';
import '../models/maintenance_cookie.dart';
import 'login_page.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        children: [
          const Expanded(
            flex: 1,
            child: BrandColumn(),
          ),
          Expanded(
            flex: 1,
            child: RegisterColumn(),
          ),
        ],
      ),
    );
  }
}

class RegisterColumn extends StatefulWidget {

  final nameController = TextEditingController();

  RegisterColumn({super.key});

  @override
  State<RegisterColumn> createState() => _RegisterColumnState();
}

class _RegisterColumnState extends State<RegisterColumn> {

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        color: const Color.fromARGB(255, 230, 230, 230),
        padding: const EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                    onTap: () => Navigator.pop(context),
                    child: const Icon(Icons.keyboard_arrow_left_rounded)),
                const Text('Login'),
              ],
            ),
            const SizedBox(height: 20),
            const Text(
              'Create a New Admin Account',
              style: TextStyle(fontSize: 20.0),
            ),
            const SizedBox(height: 20),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.white),
              child: TextField(
                controller: widget.nameController,
                textCapitalization: TextCapitalization.words,
                onChanged: (name) {
                  final text = name
                      .toLowerCase()
                      .split(' ')
                      .map((word) =>
                  word.length > 1
                      ? '${word[0].toUpperCase()}${word.substring(1)}'
                      : word.toUpperCase())
                      .join(' ');
                  widget.nameController.value = TextEditingValue(
                    text: text,
                    selection: TextSelection.collapsed(offset: text.length),
                  );

                  context.read<CredentialCubit>().nameChanged(name);
                },
                keyboardType: TextInputType.name,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Full Name',
                ),
              ),
            ),
            const SizedBox(height: 10),
            _EmailInput(),
            const SizedBox(height: 10),
            _PasswordInput(),
            const SizedBox(height: 10),
            Container(
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(10.0))),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: ModeDropDownButton(),
              ),
            ),
            const SizedBox(height: 20),
            const _RegisterButton()
          ],
        ),
      ),
    );
  }
}

class _EmailInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CredentialCubit, CredentialState>(
      buildWhen: (previous, current) => previous.email != current.email,
      builder: (context, state) {
        return Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0), color: Colors.white),
          child: TextField(
            onChanged: (email) =>
                context.read<CredentialCubit>().emailChanged(email),
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              border: const OutlineInputBorder(),
              hintText: 'Email',
              errorText: state.email.invalid ? 'Invalid Email' : null,
            ),
          ),
        );
      },
    );
  }
}

class _PasswordInput extends StatefulWidget {
  @override
  _PasswordInputState createState() => _PasswordInputState();
}

class _PasswordInputState extends State<_PasswordInput> {
  bool _obscureText = true;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CredentialCubit, CredentialState>(
      buildWhen: (previous, current) => previous.password != current.password,
      builder: (context, state) {
        return Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.white,
          ),
          child: Stack(
            alignment: Alignment.centerRight,
            children: [
              TextField(
                onChanged: (password) =>
                    context.read<CredentialCubit>().passwordChanged(password),
                obscureText: _obscureText,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  hintText: 'Password',
                  errorText: state.password.invalid
                      ? 'Minimum of 7 characters and a digit'
                      : null,
                ),
              ),
              IconButton(
                onPressed: () {
                  setState(() {
                    _obscureText = !_obscureText;
                  });
                },
                icon: Icon(
                    _obscureText ? Icons.visibility : Icons.visibility_off),
              ),
            ],
          ),
        );
      },
    );
  }
}

class _RegisterButton extends StatelessWidget {
  const _RegisterButton({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocListener<CredentialCubit, CredentialState>(
      listener: (context, state) {
        final authUid = state.uid;
        final adminType = context.read<CredentialCubit>().state.adminType;

        if (authUid.isNotEmpty) {
          final admin = Admin(
              authId: authUid,
              email: state.email.value,
              name: state.name,
              adminType: AdminTypeReadable(adminType).adminTypeStr);
          context.read<RealtimeDatabaseCubit>().writeAdmin(admin);
          context.read<CredentialCubit>().setAdmin(admin);

          CookieManager.addToCookie(
            CookieKeys.adminKey,
            admin.toStringJson(),
          );

          if (state.formzStatus == FormzStatus.valid) {
            Navigator.pop(context);
            html.window.location.reload();
          }
        }
      },
      child: BlocBuilder<CredentialCubit, CredentialState>(
          buildWhen: (previous, current) =>
              previous.formzStatus != current.formzStatus,
          builder: (context, state) {
            return state.formzStatus.isSubmissionInProgress
                ? const CircularProgressIndicator()
                : ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                      backgroundColor: Constant().pasaYellowColor,
                    ),
                    onPressed: () {
                      if (!state.formzStatus.isValidated) {
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                                content: Text(
                                    'Please enter your email and password')));
                        return;
                      }

                      final name = context.read<CredentialCubit>().state.name;
                      final adminType =
                          context.read<CredentialCubit>().state.adminType;

                      if (name.isEmpty) {
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                                content: Text('Please enter your full name')));
                        return;
                      }

                      if (adminType == AdminType.notSet) {
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                                content: Text(
                                    'Please select the type of the Admin')));
                        return;
                      }

                      context.read<CredentialCubit>().registerAdmin();
                    },
                    child: const Text('REGISTER',
                        style: TextStyle(color: Colors.black)),
                  );
          }),
    );
  }
}
