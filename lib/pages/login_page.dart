import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:formz/formz.dart';

import 'package:pasa_admin/bloc/login/credential_cubit.dart';
import 'package:pasa_admin/bloc/rtdb/rtdb_cubit.dart';
import 'package:pasa_admin/pages/register_page.dart';

import 'package:rtdb_repo/models/admin.dart';

import '../constants/constants.dart';
import '../cookie_manager.dart';
import '../models/maintenance_cookie.dart';

class LoginPage extends StatefulWidget {
  static Page<void> page() => const MaterialPage<void>(child: LoginPage());

  const LoginPage({super.key});



  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {



  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Row(
      children: const [
       Expanded(
          flex: 1,
          child: BrandColumn(),
        ),
        Expanded(
          flex: 1,
          child: LoginColumn(),
        ),
      ],
    ));
  }

}

class BrandColumn extends StatelessWidget {
  const BrandColumn({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Constant().pasaBlueColor,
      padding: const EdgeInsets.all(20),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Transform.scale(
              scale: 1.5,
              child: Image(
                  image: AssetImage(Constant().pasaIcon)
              ),
            ),
            const SizedBox(height: 4.0),
            const Text(
              "PASA",
              style: TextStyle(
                fontSize: 36,
                fontWeight: FontWeight.bold,
                color: Colors.yellow,
              ),
            ),
            const SizedBox(height: 4.0),
            const Text(
              "Patient Assistance Software App",
              style: TextStyle(color: Colors.white),
            ),
          ],
        ),
      ),
    );
  }
}

class LoginColumn extends StatefulWidget {
  const LoginColumn({super.key});

  @override
  State<LoginColumn> createState() => _LoginColumnState();
}

class _LoginColumnState extends State<LoginColumn> {
  bool _isLoggingIn = false;
  String adminModeDropdownValue = Constant().adminModes.first;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        color: const Color.fromARGB(255, 230, 230, 230),
        padding: const EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image(image: AssetImage(Constant().checkListIcon)),
            const SizedBox(height: 20),
            _EmailInput(),
            const SizedBox(height: 10),
            const _LoginPasswordInput(),
            const SizedBox(height: 20),
            Container(
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ModeDropDownButton(),
                  ),
            ),
            const SizedBox(height: 20),
            InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return const RegisterPage();
                    }),
                  );
                },
                child: const Text(
                  'Create A New Admin',
                  style: TextStyle(
                    decoration: TextDecoration.underline
                  ),
              ),
            ),
            const SizedBox(height: 20),
            _LoginButton(),
          ],
        ),
      ),
    );
  }
}


class ModeDropDownButton extends StatelessWidget {

  final _items = [
    AdminTypeReadable(AdminType.notSet).adminTypeStr,
    AdminTypeReadable(AdminType.nurse).adminTypeStr,
    AdminTypeReadable(AdminType.maintenance).adminTypeStr,
  ];

  ModeDropDownButton({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CredentialCubit, CredentialState>(
      buildWhen: (prev, current) => prev.adminType != current.adminType,
      builder: (context, state) {
        return DropdownButton(
          onChanged: (value) {
            context.read<CredentialCubit>().setAdminType(
              _strToAdminType(value!)
            );
          },
          value: AdminTypeReadable(state.adminType).adminTypeStr,
          items: _items.map((adminTypeStr) {
            return DropdownMenuItem(
              value: adminTypeStr,
              child: Text(adminTypeStr),
            );
          }).toList(),
        );
      },
    );
  }

  _strToAdminType(String adminTypeStr) {
    switch (adminTypeStr) {
      case 'Select':
        return AdminType.notSet;
      case 'Nurse':
        return AdminType.nurse;
      case 'Maintenance':
        return AdminType.maintenance;
    }
  }

}

class _EmailInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CredentialCubit, CredentialState>(
      buildWhen: (previous, current) => previous.email != current.email,
      builder: (context, state) {
        return Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0), color: Colors.white),
          child: TextField(
            onChanged: (email) => context.read<CredentialCubit>().emailChanged(email),
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              border: const OutlineInputBorder(),
              hintText: 'Email',
              helperText: state.email.invalid ? 'Please enter a valid email address' : null,
            ),
          ),
        );
      },
    );
  }
}


class _LoginPasswordInput extends StatefulWidget {
  const _LoginPasswordInput({super.key});

  @override
  _LoginPasswordInputState createState() => _LoginPasswordInputState();
}

class _LoginPasswordInputState extends State<_LoginPasswordInput> {
  bool _obscureText = true;
  final TextEditingController _passwordController = TextEditingController();

  @override
  void dispose() {
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0), color: Colors.white),
        child: TextField(
          controller: _passwordController,
          onChanged: (password) => context.read<CredentialCubit>().passwordChanged(password),
          obscureText: _obscureText,
          decoration: InputDecoration(
            border: const OutlineInputBorder(),
            hintText: 'Password',
            suffixIcon: IconButton(
              icon: Icon(_obscureText ? Icons.visibility : Icons.visibility_off),
              onPressed: () {
                setState(() {
                  _obscureText = !_obscureText;
                });
              },
            ),
          ),
        ),
    );
  }
}

class _LoginButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocListener<RealtimeDatabaseCubit, RealtimeDatabaseState>(
      listener: (context, state) {
        final admin = state.admin;

        final verifiedAdminType = AdminTypeReadable.serialize(admin?.adminType);
        final requestedAdminType = context.read<CredentialCubit>().state.adminType;

        if (requestedAdminType != AdminType.notSet && requestedAdminType == verifiedAdminType) {
          CookieManager.addToCookie(
            CookieKeys.adminKey,
            admin!.toStringJson()
          );
          CookieManager.addToCookie(
              CookieKeys.techMaintKey,
              MaintenanceCookie(
                  techRequestsCount: state.technicianMaintenanceRequestList?.length ?? 0,
              ).toStringJson()
          );
          CookieManager.addToCookie(
              CookieKeys.houseMaintKey,
              MaintenanceCookie(
                housekeepingRequestsCount: state.housekeepingMaintenanceRequestList?.length ?? 0,
              ).toStringJson()
          );


          context.read<CredentialCubit>().logInWithCredentials();
          return;
        }
      },
      child: BlocListener<CredentialCubit, CredentialState>(
        listener: (context, state) {
          switch (state.formzStatus) {
            case FormzStatus.submissionFailure:
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text(state.errorMessage ?? 'Failed to login')
              ));
              break;
          }

        },
        child: BlocBuilder<CredentialCubit, CredentialState>(
          buildWhen: (prev, curr) => prev.formzStatus != curr.formzStatus,
          builder: (context, state) {
            return state.formzStatus.isSubmissionInProgress
                ?
                const CircularProgressIndicator()
                :
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                      backgroundColor: Constant().pasaYellowColor,
                    ),
                    onPressed: () {
                      final requestedAdminType = context.read<CredentialCubit>().state.adminType;

                      (
                        state.formzStatus.isValidated
                        &&
                        (requestedAdminType != AdminType.notSet)
                      )
                      ?
                      () {
                        context.read<RealtimeDatabaseCubit>()
                          .getAdminByEmail(state.email.value);

                        final admin = context.read<RealtimeDatabaseCubit>()
                          .state.admin;

                        final verifiedEmittedAdminType =
                          AdminTypeReadable.serialize(admin?.adminType);

                        if (verifiedEmittedAdminType == AdminType.notSet) {
                          return;
                        }

                        if (requestedAdminType != verifiedEmittedAdminType) {
                              ScaffoldMessenger.of(context)
                                .showSnackBar(
                                  SnackBar(content: Text(
                                    'Cannot log in as ${
                                      AdminTypeReadable(requestedAdminType).adminTypeStr
                                    }'
                                  ))
                              );
                            return;
                        }

                        CookieManager.addToCookie(
                          CookieKeys.adminKey,
                          admin!.toStringJson()
                        );
                        context.read<CredentialCubit>().logInWithCredentials();
                      }()
                      :
                      null;
                    },
                    child: const Text(
                      'LOGIN',
                       style: TextStyle(color: Colors.black)
                    ),
                  );
          },
        ),
      ),
    );
  }
}
