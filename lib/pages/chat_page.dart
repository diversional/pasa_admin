import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_chat_bubble/chat_bubble.dart';
import 'package:pasa_admin/bloc/notif/notif_cubit.dart';
import 'package:pasa_admin/bloc/rtdb/rtdb_cubit.dart';
import 'package:rtdb_repo/models/chat_message.dart';

class ChatPage extends StatefulWidget {

  final String patientId;
  final String? notifToken, confirmedMessageAndOtherInput;

  final textController = TextEditingController();

  ChatPage({
    required this.patientId,
    this.notifToken,
    this.confirmedMessageAndOtherInput,
    super.key
  });

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  final _scrollController = ScrollController();

  @override
  void initState() {
    context.read<RealtimeDatabaseCubit>().listenChatRoom(
        FirebaseAuth.instance.currentUser!.uid, widget.patientId);

    if (widget.confirmedMessageAndOtherInput != null) {
      widget.textController.text = widget.confirmedMessageAndOtherInput!;
      _sendMessage();
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RealtimeDatabaseCubit, RealtimeDatabaseState>(
      listener: (context, state) {
        if (state.messageList != null) {
          _scrollToBottomEnd();
        }
      },
      child: BlocBuilder<RealtimeDatabaseCubit, RealtimeDatabaseState>(
        builder: (context, state) {
          return Scaffold(
            appBar: AppBar(
              title: const Text('Chat'),
            ),
            body: Column(
              children: [
                Expanded(
                    child: state.messageList == null
                        ? const Center(child: CircularProgressIndicator())
                        : ListView.builder(
                            controller: _scrollController,
                            itemCount: state.messageList!.length,
                            itemBuilder: (context, index) {
                              final chatMessage = state.messageList![index];

                              final timeStamp = int.parse(chatMessage.id!);

                              return chatMessage.senderAssignedId ==
                                      FirebaseAuth.instance.currentUser?.uid
                                  ? Column(
                                      children: [
                                        getSenderView(timeStamp, chatMessage.body!),
                                        const SizedBox(height: 10.0)
                                      ],
                                    )
                                  : Column(
                                      children: [
                                        getReceiverView(timeStamp, chatMessage.body!),
                                        const SizedBox(height: 10.0)
                                      ],
                                    );
                            },
                          )),
                const SizedBox(height: 10.0),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 8),
                  child: Row(
                    children: [
                      Expanded(
                        child: TextField(
                          controller: widget.textController,
                          decoration: const InputDecoration(
                            hintText: 'Type a message',
                            border: OutlineInputBorder(),
                          ),
                          onSubmitted: (submitted) {
                            _sendMessage();
                          },
                        ),
                      ),
                      IconButton(
                        icon: const Icon(
                          Icons.arrow_circle_up_rounded,
                          color: Colors.green,
                        ),
                        onPressed: () => _sendMessage(),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 10.0),
              ],
            ),
          );
        },
      ),
    );
  }

  getSenderView(int time, String message) => Container(
      margin: const EdgeInsets.only(left: 10.0, right: 10.0),
      child: PasaChatBubble(
        message: message,
        readableElapsedTime: _formatTimestamp(time),
        bubbleType: BubbleType.sendBubble,
      )
  );

  getReceiverView(int time, String message) => Container(
      margin: const EdgeInsets.only(left: 10.0, right: 10.0),
      child: PasaChatBubble(
        message: message,
        readableElapsedTime: _formatTimestamp(time),
        bubbleType: BubbleType.receiverBubble,
      )
  );

  _sendMessage() {
    final message = widget.textController.text;
    if (message.isNotEmpty) {
      widget.textController.clear();

      final adminAuthId = FirebaseAuth.instance.currentUser?.uid;

      context.read<RealtimeDatabaseCubit>().sendMessage(
          adminAuthId!,
          widget.patientId,
          ChatMessage(body: message, senderAssignedId: adminAuthId));

      _scrollToBottomEnd();
    }
  }

  @override
  void dispose() {
    context.read<RealtimeDatabaseCubit>().cancelListenChatRoom();
    super.dispose();
  }

  _scrollToBottomEnd() {
    Timer(const Duration(seconds: 1), () {
      _scrollController.animateTo(
        _scrollController.position.maxScrollExtent,
        duration: const Duration(seconds: 1),
        curve: Curves.easeOut,
      );
    });
  }

  String _formatTimestamp(int timestampMillis) {
    final now = DateTime.now();
    final timestamp = DateTime.fromMillisecondsSinceEpoch(timestampMillis);
    final difference = now.difference(timestamp);

    if (difference.inDays >= 1) {
      return '${timestamp.day}/${timestamp.month}/${timestamp.year}';
    } else if (difference.inHours >= 1) {
      return '${difference.inHours}h ago';
    } else if (difference.inMinutes >= 1) {
      return '${difference.inMinutes}m ago';
    } else {
      return 'just now';
    }
  }

}

class PasaChatBubble extends StatelessWidget {

  final String message, readableElapsedTime;
  final BubbleType bubbleType;

  PasaChatBubble({
    required this.message,
    required this.readableElapsedTime,
    required this.bubbleType,
    super.key,
  });

  bool showTimestamp = false;

  @override
  Widget build(BuildContext context) {
    final isReceiver = bubbleType == BubbleType.receiverBubble;

    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: ChatBubble(
        clipper: ChatBubbleClipper2(type: bubbleType),
        backGroundColor:
        isReceiver ? const Color(0xffE7E7ED) : Colors.lightBlue,
        alignment:
        isReceiver ? Alignment.centerLeft : Alignment.topRight,
        margin: const EdgeInsets.only(top: 10.0),
        child: Container(
          constraints: BoxConstraints(
            maxWidth: MediaQuery.of(context).size.width * 0.7,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                message,
                style: TextStyle(
                  color: isReceiver ? Colors.black : Colors.white,
                ),
              ),

              const SizedBox(height: 2.0),
              (!isReceiver && readableElapsedTime == 'just now') ?
              const SizedBox()
                  :
              Text(
                readableElapsedTime,
                style: TextStyle(
                  color: isReceiver ? Colors.black45 : Colors.white70,
                  fontSize: 12.0,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
