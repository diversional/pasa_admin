import 'dart:ui';

class Constant {

  static final Constant _instance = Constant._privateConstructor();

  factory Constant() { return _instance; }

  Constant._privateConstructor();

  final loginRoute = '/login';
  final dashboardRoute = '/dashboard';

  final pasaIcon = 'assets/images/pasa_icon.png';
  final checkListIcon = 'assets/images/checklist.png';
  final alertSound = '/assets/assets/audios/room_alert.mp3';
  final radioButtonTone = '/assets/assets/audios/radiobutton_notification.mp3';
  final chatTone = '/assets/assets/audios/chat_ringtone.mp3';

  final adminModes = ['Select', 'Nurse', 'Maintenance'];

  // Fragment keywords
  final popupMenuItems = ['Details', 'Chat', 'Update', 'Delete', 'Clear Conversation'];

  final nurseFragmentTitles = ['Dashboard', 'Patients', 'Pending Requests'];
  final maintenanceFragmentTitles = ['Technician', 'Housekeeping'];
  // Fragment keywords

  final pasaBlueColor = const Color.fromARGB(0xff, 0x11, 0x34, 0x50);
  final pasaYellowColor = const Color.fromARGB(0xff, 255, 176, 1);


}