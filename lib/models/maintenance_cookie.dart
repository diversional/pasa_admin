import 'dart:convert';

class MaintenanceCookie {

  final int? techRequestsCount, housekeepingRequestsCount;

  MaintenanceCookie({this.techRequestsCount, this.housekeepingRequestsCount});

  factory MaintenanceCookie.fromJson(Map<String, dynamic> json) {
    return MaintenanceCookie(
      techRequestsCount: json['techRequestsCount'],
      housekeepingRequestsCount: json['housekeepingRequestsCount'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'techRequestsCount': techRequestsCount,
      'housekeepingRequestsCount': housekeepingRequestsCount,
    };
  }

  String toStringJson() {
    final mapJson = toJson();
    return json.encode(mapJson);
  }

  static Map<String, dynamic> toMapJson(String deserJson) {
    return json.decode(deserJson);
  }

}