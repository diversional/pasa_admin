import 'dart:convert';

class ChatCookie {

  final String? keyAndSenderPatientId;
  final int? messageLength;

  ChatCookie({
    this.keyAndSenderPatientId,
    this.messageLength,
  });

  factory ChatCookie.fromJson(Map<String, dynamic> json) {
    return ChatCookie(
      keyAndSenderPatientId: json['keyAndSenderPatientId'],
      messageLength: json['messageLength'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'keyAndSenderPatientId': keyAndSenderPatientId,
      'messageLength': messageLength,
    };
  }

  String toStringJson() {
    final mapJson = toJson();
    return json.encode(mapJson);
  }

  static Map<String, dynamic> toMapJson(String deserJson) {
    return json.decode(deserJson);
  }

}