import 'package:universal_html/html.dart' as html;

class CookieKeys {
  static const adminKey = 'ak';
  static const techMaintKey = 'tech_count_maint_key';
  static const houseMaintKey = 'house_count_maint_key';
}

class CookieManager {

  static addToCookie(String key, String value) {
     // 2592000 sec = 30 days.
     html.document.cookie = "$key=$value; max-age=2592000; path=/;";
  }

  static String getCookie(String key) {
    String cookies = html.document.cookie ?? '';
    List<String> listValues = cookies.isNotEmpty ? cookies.split(";") : [];
    String matchVal = "";
    for (int i = 0; i < listValues.length; i++) {
      List<String> map = listValues[i].split("=");
      String _key = map[0].trim();
      String _val = map[1].trim();
      if (key == _key) {
        matchVal = _val;
        break;
      }
    }
    return matchVal;
  }

}