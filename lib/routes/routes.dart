import 'package:flutter/material.dart';
import 'package:pasa_admin/bloc/auth/auth_bloc.dart';

import '../pages/dashb_page.dart';
import '../pages/login_page.dart';

List<Page<dynamic>> onGenerateAppViewPages(
  AuthStatus authStatus,
  List<Page<dynamic>> pages,
) {
  switch (authStatus) {
    case AuthStatus.authed:
      return [DashbPage.page()];
    case AuthStatus.unauthed:
      return [LoginPage.page()];
  }
}