import 'package:auth_repo/auth_repo.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flow_builder/flow_builder.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pasa_admin/bloc/notif/notif_cubit.dart';
import 'package:pasa_admin/bloc/rtdb/rtdb_cubit.dart';

import 'package:pasa_admin/routes/routes.dart';
import 'package:rtdb_repo/notif_repo.dart';
import 'package:rtdb_repo/rtdb_repo.dart';

import 'bloc/auth/auth_bloc.dart';
import 'bloc/login/credential_cubit.dart';
import 'firebase_options.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);

  final authRepo = AuthRepo();
  await authRepo.user.first;

  final rtdbRepo = RealtimeDatabaseRepo();
  final notifRepo = NotifRepo();

  runApp(PasaApp(
    authRepo: authRepo,
    realtimeDatabaseRepo: rtdbRepo,
    notifRepo: notifRepo,
  ));
}

class PasaApp extends StatelessWidget {

  final AuthRepo _authRepo;
  final RealtimeDatabaseRepo _realtimeDatabaseRepo;
  final NotifRepo _notifRepo;

  const PasaApp({
    required AuthRepo authRepo,
    required RealtimeDatabaseRepo realtimeDatabaseRepo,
    required NotifRepo notifRepo,
    super.key
  }) :
  _authRepo = authRepo,
  _realtimeDatabaseRepo = realtimeDatabaseRepo,
  _notifRepo = notifRepo;

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider(create: (_) => _authRepo),
        RepositoryProvider(create: (_) => _realtimeDatabaseRepo),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider(create: (_) => AuthBloc(authRepo: _authRepo)),
          BlocProvider(create: (_) => CredentialCubit(_authRepo)),
          BlocProvider(create: (_) => RealtimeDatabaseCubit(_realtimeDatabaseRepo)),
          BlocProvider(create: (_) => NotifCubit(notifRepo: _notifRepo)),
        ],
        child: const PasaMaterialApp(),
      )
    );
  }
}

class PasaMaterialApp extends StatelessWidget {
  const PasaMaterialApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PASA Admin',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: FlowBuilder<AuthStatus>(
        state: context.select((AuthBloc bloc) => bloc.state.authStatus),
        onGeneratePages: onGenerateAppViewPages,
      ),
    );
  }

}
