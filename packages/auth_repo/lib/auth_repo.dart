import 'package:cache/cache.dart';
import 'package:firebase_auth/firebase_auth.dart' as firebase_auth;
import 'package:meta/meta.dart';
import 'package:rtdb_repo/models/admin.dart';

import 'auth_exception.dart';

class AuthRepo {

  final firebase_auth.FirebaseAuth _firebaseAuth;
  final CacheClient _cacheClient;

  AuthRepo({
    firebase_auth.FirebaseAuth? firebaseAuth,
    CacheClient? cacheClient,
  }) : 
      _cacheClient = cacheClient ?? CacheClient(),
      _firebaseAuth = firebaseAuth ?? firebase_auth.FirebaseAuth.instance;

  /// User cache key.
  /// Should only be used for testing purposes.
  @visibleForTesting
  static const userCacheKey = '__user_cache_key__';

  Stream<Admin> get user {
    return _firebaseAuth.authStateChanges().map((firebaseUser) {
      final admin = firebaseUser == null ? Admin.empty : firebaseUser.toAdmin;
      _cacheClient.write(key: userCacheKey, value: admin);
      return admin;
    });
  }

  /// Returns the current cached Admin.
  /// Defaults to [Admin.empty] if there is no cached user.
  Admin get currentUser {
    return _cacheClient.read<Admin>(key: userCacheKey) ?? Admin.empty;
  }

  Future<void> logInWithEmailAndPassword({
    required String email,
    required String pass,
  }) async {
    try {
      await _firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: pass,
      );
    } on firebase_auth.FirebaseAuthException catch (e) {
      throw AuthLoginException.fromCode(e.code);
    } catch (_) {
      throw const AuthLoginException();
    }
  }

  Future<void> logOut() async {
    try {
      await Future.wait([_firebaseAuth.signOut()]);
    } catch (_) {
      throw LogOutFailure();
    }
  }

  Future<void> registerWithEmailAndPassword({
    required String email,
    required String pass,
  }) async {
    try {
      await _firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: pass,
      );
    } on firebase_auth.FirebaseAuthException catch (e) {
      throw AuthLoginException.fromCode(e.code);
    } catch (_) {
      throw const AuthLoginException();
    }
  }

}

extension on firebase_auth.User {
  Admin get toAdmin {
    return Admin(
      authId: uid,
      email: email,
      name: displayName,
    );
  }
}