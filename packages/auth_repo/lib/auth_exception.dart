
class LogOutFailure implements Exception {}

/// {@template log_in_with_email_and_password_failure}
/// Thrown during the login process if a failure occurs.
/// https://pub.dev/documentation/firebase_auth/latest/firebase_auth/FirebaseAuth/signInWithEmailAndPassword.html
/// {@endtemplate}
class AuthLoginException implements Exception {
  
  /// {@macro log_in_with_email_and_password_failure}
  const AuthLoginException([
    this.message = 'An unknown exception occurred.',
  ]);

  /// Create an authentication message
  /// from a firebase authentication exception code.
  factory AuthLoginException.fromCode(String code) {
    switch (code) {
      case 'invalid-email':
        return const AuthLoginException(
          'Email is not valid or badly formatted.',
        );
      case 'user-disabled':
        return const AuthLoginException(
          'This user has been disabled. Please contact support for help.',
        );
      case 'user-not-found':
        return const AuthLoginException(
          'Email is not found, please create an account.',
        );
      case 'wrong-password':
        return const AuthLoginException(
          'Incorrect password, please try again.',
        );
      default:
        return const AuthLoginException();
    }
  }

  /// The associated error message.
  final String message;

}