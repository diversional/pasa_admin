class RealtimeDatabaseException implements Exception {

  String? message;

  RealtimeDatabaseException({
    this.message = 'RTDB error occured'
  });

}