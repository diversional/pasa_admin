import 'package:firebase_messaging/firebase_messaging.dart';

class NotifRepo {

  final FirebaseMessaging _messaging = FirebaseMessaging.instance;

  Future<String?> getToken() async {
    return await _messaging.getToken();
  }

  Future<void> sendNotificationToToken(String title, String body, String token) async {
    await _messaging.sendMessage(
      to: token,
      data: {
        'title': title,
        'body': body,
      }
    );
  }

  Future<bool> requestNotificationPermission() async {
    NotificationSettings settings = await _messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    return settings.authorizationStatus == AuthorizationStatus.authorized;
  }

}