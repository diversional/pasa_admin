import 'package:rtdb_repo/models/chat_message.dart';
import 'package:rtdb_repo/models/patient.dart';
import 'package:rtdb_repo/models/pending_room_alert.dart';
import 'package:rtdb_repo/models/request_maintenance.dart';
import 'package:rtdb_repo/rtdb_ref.dart';

import 'models/admin.dart';
import 'models/room.dart';

class RealtimeDatabaseRepo {
  final _databaseReferences = RealtimeDatabaseRef();

  // READ

  // Admin
  Future<Admin?> getAdminByEmail(String email) async {
    final event = await _databaseReferences.adminRoot.orderByChild('email')
      .equalTo(email)
      .once();
    final data = event.snapshot.value as Map<dynamic, dynamic>?;
    if (data != null) {
      final admin = data.values.first;
      return Admin.fromJson(admin);
    }
    return null;
  }

  Future<Admin?> getAdmin(String uid) async {
    final snapshot = await _databaseReferences.getAdmin(uid).get();
    if (snapshot.exists) {
      return Admin.fromJson(snapshot.value as Map<String, dynamic>);
    }
    return null;
  }
  // Admin

  // Room
  Stream<List<Room>> get allRoomsStream {
    return _databaseReferences.roomRoot.onValue.map((event) {
      
      if (event.snapshot.value == null) {
        return [];
      }

      final result = event.snapshot.value as Map<String, dynamic>;

      return result.entries.map((entry) {
        final room = entry.value;

        return Room(
          number: room['number'],
          bedCount: room['bedCount'],
          floor: room['floor']
        );
      }).toList();
    });
  }

  // Patient
  Stream<List<Patient>> get allPatientsStream {
    return _databaseReferences.patientRoot.onValue.map((event) {
      final result = event.snapshot.value as Map<String, dynamic>;

      if (event.snapshot.value == null) {
        return [];
      }
     
      return result.entries.map((entry) {
        final patient = entry.value;

        return Patient(
          id: patient['id'],
          authId: patient['authId'],
          roomNumber: patient['roomNumber'],
          name: patient['name'],
          gender: patient['gender'],
          bedNumber: patient['bedNumber'],
          reason: patient['reason'],
          notifToken: patient['notifToken'],
          age: patient['age'],
          civilStatus: patient['civilStatus'],
          religion: patient['religion'],
          dateOfAdmission: patient['dateOfAdmission'],
          chiefComplaint: patient['chiefComplaint'],
          tempPass: patient['tempPass'],
        );

      }).toList();
    });
  }

  Future<bool> canDeleteRoom(String roomNumber) async {
    final patient = await getPatientByRoomNumber(roomNumber);
    
    if (patient == null) {
      await _databaseReferences.getRoom(roomNumber).remove();
      return true;
    }
    return false;
  }
  
  Future<Patient?> getPatientByRoomNumber(String roomNumber) async {
    final event = await _databaseReferences.patientRoot.orderByChild('roomNumber')
      .equalTo(roomNumber)
      .once();
    final data = event.snapshot.value as Map<dynamic, dynamic>?;
    if (data != null) {
      final patient = data.values.first;
      return Patient.fromJson(patient);
    }
    return null;
  }
  // Patient

  // Chat
  Stream<List<ChatMessage>> getChatRoom(String adminAuthId, String patientId) {
    return _databaseReferences.getAdminPatientConversations(adminAuthId, patientId).onValue.map((event) {
      
      if (event.snapshot.value == null) {
        return [];
      }

      final result = event.snapshot.value as Map<String, dynamic>;

      return result.entries.map((entry) {
        final chatMessage = entry.value;

        return ChatMessage(
          id: chatMessage['id'],
          body: chatMessage['body'],
          senderAssignedId: chatMessage['senderAssignedId'],
        );
      }).toList();
    });
  }

  // Chat

  // Maintenance
  // technician
  Stream<List<RequestMaintenance>> get technicianRequestsMaintenanceStream {
    return _databaseReferences.technicianMaintenanceRoot.onValue.map((event) {

      if (event.snapshot.value == null) {
        return [];
      }

      final result = event.snapshot.value as Map<String, dynamic>;

      return result.entries.map((entry) => RequestMaintenance(
        timeStampAsId: entry.value['timeStampAsId'],
        patientId: entry.value['patientId'],
        message: entry.value['message'],
        type: entry.value['type']
      )).toList();
    });
  }
  Future<bool> deleteTechnicianRequest(String timeStampAsId) async {
    try {
      _databaseReferences.getTechnicianRequest(timeStampAsId).remove();
      return true;
    } catch (error) {
      print('Error deleting request: $error');
      return false;
    }
  }

  Future<void> deleteTechnicianByPatientId(String patientId) async {
    try {
      final event = await _databaseReferences.technicianMaintenanceRoot.orderByChild('patientId')
          .equalTo(patientId)
          .once();
      final data = event.snapshot.value as Map<dynamic, dynamic>?;
      if (data != null) {
        data.values.forEach((element) {
          final result = RequestMaintenance.fromJson(element);
          deleteTechnicianRequest(result.timeStampAsId ?? '');
        });
      }
    } catch (error) {
      print('Error deleting technician: $error');
    }
  }
  // technician

  // housekeeping
  Stream<List<RequestMaintenance>> get housekeepingRequestsMaintenanceStream {
    return _databaseReferences.housekeepingMaintenanceRoot.onValue.map((event) {

      if (event.snapshot.value == null) {
        return [];
      }

      final result = event.snapshot.value as Map<String, dynamic>;

      return result.entries.map((entry) => RequestMaintenance(
        timeStampAsId: entry.value['timeStampAsId'],
        patientId: entry.value['patientId'],
        message: entry.value['message'],
        type: entry.value['type']
      )).toList();
    });
  }
  Future<bool> deleteHousekeepingRequest(String timeStampAsId) async {
    try {
      _databaseReferences.getHousekeepingRequest(timeStampAsId).remove();
      return true;
    } catch (error) {
      print('Error deleting request: $error');
      return false;
    }
  }

  Future<void> deleteHousekeepingByPatientId(String patientId) async {
    try {
      final event = await _databaseReferences.housekeepingMaintenanceRoot.orderByChild('patientId')
          .equalTo(patientId)
          .once();
      final data = event.snapshot.value as Map<dynamic, dynamic>?;
      if (data != null) {
        data.values.forEach((element) {
          final result = RequestMaintenance.fromJson(element);
          deleteHousekeepingRequest(result.timeStampAsId ?? '');
        });
      }
    } catch (error) {
      print('Error deleting housekeeping: $error');
    }
  }
  // housekeeping
  // Maintenance

  Stream<List<PendingRoomAlert>> get pendingEmergencyOrAssistanceAlerts {
    return _databaseReferences.alertRoot.onValue.map((event) {

      if (event.snapshot.value == null) {
        return [];
      }

      final result = event.snapshot.value as Map<String, dynamic>;

      return result.entries.map((entry) {
        final alert = entry.value;

        return PendingRoomAlert(
          patientId: alert['patientId'],
          message: alert['message'],
          name: alert['name'],
          roomNumber: alert['roomNumber'],
          bedNumber: alert['bedNumber'],
          roomAlertType: alert['roomAlertType'],
        );
      }).toList();
    });
  }

  Stream<List<PendingRoomAlert>> get listenHistoryPendingRoomAlert {
    return _databaseReferences.historyAlertRoot.onValue.map((event) {

      if (event.snapshot.value == null) {
        return [];
      }

      final result = event.snapshot.value as Map<String, dynamic>;

      return result.entries.map((entry) {
        final alert = entry.value;

        return PendingRoomAlert(
          patientId: alert['patientId'],
          message: alert['message'],
          name: alert['name'],
          roomNumber: alert['roomNumber'],
          bedNumber: alert['bedNumber'],
          roomAlertType: alert['roomAlertType'],
          timeStamp: alert['timeStamp'],
        );
      }).toList();
    });
  }
  // READ

  // WRITE
  Future<bool> writeAdmin(Admin admin) async {
    try {
      await _databaseReferences.getAdmin(admin.authId!)
        .set(admin.toJson());
      return true;
    } catch (error) {
      print('Error writing admin: $error');
      return false;
    }
  }

  Future<bool> writeHistoryPendingRoomAlert(PendingRoomAlert roomAlert) async {
    final timeStamp = DateTime.now().millisecondsSinceEpoch;
    final historifiedPendingRoomAlert = roomAlert.copyWith(timeStamp: timeStamp);
    try {
      final delHistoryPendingRoomAlert = _databaseReferences.getHistoryPendingAlert(timeStamp.toString());
      await delHistoryPendingRoomAlert.set(historifiedPendingRoomAlert.toJson());
      return true;
    } catch (error) {
      print('Error writing history alert: $error');
      return false;
    }
  }

  Future<void> deleteHistoryPendingRoomAlert(String timeStamp) async {
    try {
      final delHistoryPendingRoomAlert = _databaseReferences.getHistoryPendingAlert(timeStamp);
      await delHistoryPendingRoomAlert.remove();
    } catch (error) {
      print('Error removing history alert: $error');
    }
  }


  Future<bool> deleteRoomAlert(String patientId) async {
    try {
      await _databaseReferences.getAlert(patientId).remove();
      return true;
    } catch (error) {
      return false;
    }
  }

  Future<void> deleteAdminAndPatientConvo(String adminAuthId, String patientId) async {
    try {
      final chatRoomRef = _databaseReferences.getAdminPatientConversations(adminAuthId, patientId);
      await chatRoomRef.remove();
    } catch (error) {
      print('Error removing chat room: $error');
    }
  }

  Future<void> deletePatientChatRoom(String patientId) async {
    try {
      final chatRoomRef = _databaseReferences.getPatientConversationsRoot(patientId);
      await chatRoomRef.remove();
    } catch (error) {
      print('Error removing chat room: $error');
    }
  }

  Future<bool> sendMessage(String adminAuthId, String patientId, ChatMessage message) async {
    try {
      final timeStampId = DateTime.now().millisecondsSinceEpoch.toString();
      final chatRef = _databaseReferences.getAdminPatientConversations(adminAuthId, patientId)
        .child(timeStampId);

      await chatRef.set(message.copyWith(
        id: timeStampId
      ).toJson());
      return true;
    } catch (error) {
      print('Error sending message: $error');
      return false;
    }
  }

  Future<bool> writePatient(Patient patient, String id, {required bool update}) async {
    print('patient= ${patient.toJson()}');
    print('id= ${id}');
    try {
      final patientRef = _databaseReferences.getPatient(id);
      if (update) {
        await patientRef.update(patient.toJson());
      } else {
        await patientRef.set(patient.toJson());
      }
      return true;
    } catch (error) {
      print('Error writing patient: $error');
      return false;
    }
  }

  Future<bool> deletePatient(Patient patient) async {
    try {
      final patientRef = _databaseReferences.getPatient(
        patient.authId ?? patient.id!
      );
      await patientRef.remove();
      return true;
    } catch (error) {
      print('Error deleting patient: $error');
      return false;
    }
  }

  Future<bool> writeRoom(Room room) async {
    try {
      final roomRef = _databaseReferences.roomRoot.child(room.number!);
      await roomRef.set(room.toJson());
      return true;
    } catch (error) {
      print('Error writing room: $error');
      return false;
    }
  }
  // WRITE

  
}
