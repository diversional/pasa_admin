import 'package:equatable/equatable.dart';

class Patient extends Equatable {

  final String id;
  final String authId;
  final String roomNumber;
  final String name;
  final String gender;
  final int bedNumber;
  final String reason;
  final String notifToken;
  final int age;
  final String civilStatus;
  final String religion;
  final String dateOfAdmission;
  final String chiefComplaint;
  final String tempPass;

  Patient({
    this.id = "",
    this.authId = "",
    this.roomNumber = "",
    this.name = "",
    this.gender = "",
    this.bedNumber = 0,
    this.reason = "",
    this.notifToken = "",
    this.age = 0,
    this.civilStatus = "",
    this.religion = "",
    this.dateOfAdmission = "",
    this.chiefComplaint = "",
    this.tempPass = "",
  });

  @override
  List<Object?> get props =>
      [
        id,
        authId,
        roomNumber,
        name,
        gender,
        bedNumber,
        reason,
        notifToken,
        age,
        civilStatus,
        religion,
        dateOfAdmission,
        chiefComplaint,
        tempPass
      ];

  factory Patient.fromJson(Map<String, dynamic> json) {
    return Patient(
      id: json['id'] ?? "",
      authId: json['authId'] ?? "",
      roomNumber: json['roomNumber'] ?? "",
      name: json['name'] ?? "",
      gender: json['gender'] ?? "",
      bedNumber: json['bedNumber'] ?? 0,
      reason: json['reason'] ?? "",
      notifToken: json['notifToken'] ?? "",
      age: json['age'] ?? 0,
      civilStatus: json['civilStatus'] ?? "",
      religion: json['religion'] ?? "",
      dateOfAdmission: json['dateOfAdmission'] ?? "",
      chiefComplaint: json['chiefComplaint'] ?? "",
      tempPass: json['tempPass'] ?? "",
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'authId': authId,
      'roomNumber': roomNumber,
      'name': name,
      'gender': gender,
      'bedNumber': bedNumber,
      'reason': reason,
      'notifToken': notifToken,
      'age': age,
      'civilStatus': civilStatus,
      'religion': religion,
      'dateOfAdmission': dateOfAdmission,
      'chiefComplaint': chiefComplaint,
      'tempPass': tempPass,
    };
  }

  Map<String, dynamic> toReadableProperties() {
    return {
      'Room': roomNumber,
      'Name': name,
      'Gender': gender,
      'Bed': bedNumber,
      'Reason': reason,
      'Age': age,
      'Civil Status': civilStatus,
      'Religion': religion,
      'Date of Admission': dateOfAdmission,
      'Chief Complaint': chiefComplaint,
      'Temporary Password': tempPass,
    };
  }

  Patient copyWith({
    String? id,
    String? authId,
    String? roomNumber,
    String? name,
    String? gender,
    int? bedNumber,
    String? reason,
    String? notifToken,
    int? age,
    String? civilStatus,
    String? religion,
    String? dateOfAdmission,
    String? chiefComplaint,
    String? tempPass,
  }) {
    return Patient(
      id: id ?? this.id,
      authId: authId ?? this.authId,
      roomNumber: roomNumber ?? this.roomNumber,
      name: name ?? this.name,
      gender: gender ?? this.gender,
      bedNumber: bedNumber ?? this.bedNumber,
      reason: reason ?? this.reason,
      notifToken: notifToken ?? this.notifToken,
      age: age ?? this.age,
      civilStatus: civilStatus ?? this.civilStatus,
      religion: religion ?? this.religion,
      dateOfAdmission: dateOfAdmission ?? this.dateOfAdmission,
      chiefComplaint: chiefComplaint ?? this.chiefComplaint,
      tempPass: tempPass ?? this.tempPass,
    );
  }

}


