import 'package:equatable/equatable.dart';

class PendingRoomAlert extends Equatable {

  final String patientId;
  final String message;
  final String name;
  final String roomNumber;
  final int bedNumber;
  final int? roomAlertType;
  final int timeStamp;

  PendingRoomAlert({
    required this.patientId,
    required this.message,
    required this.name,
    required this.roomNumber,
    required this.bedNumber,
    required this.roomAlertType,
    this.timeStamp = 0
  });

  Map<String, dynamic> toJson() {
    return {
      'patientId': patientId,
      'message': message,
      'name': name,
      'roomNumber': roomNumber,
      'bedNumber': bedNumber,
      'roomAlertType': roomAlertType,
      'timeStamp': timeStamp,
    };
  }

  Map<String, dynamic> toReadableProperties() {
    return {
      'Patient ID': patientId,
      'Message': message,
      'Name': name,
      'Room': roomNumber,
      'Bed': bedNumber,
      'Timestamp': timeStamp,
    };
  }

  PendingRoomAlert copyWith({
    String? patientId,
    String? message,
    String? name,
    String? roomNumber,
    int? bedNumber,
    int? roomAlertType,
    int? timeStamp,
  }) {
    return PendingRoomAlert(
      patientId: patientId ?? this.patientId,
      message: message ?? this.message,
      name: name ?? this.name,
      roomNumber: roomNumber ?? this.roomNumber,
      bedNumber: bedNumber ?? this.bedNumber,
      roomAlertType: roomAlertType ?? this.roomAlertType,
      timeStamp: timeStamp ?? this.timeStamp,
    );
  }

  @override
  List<Object?> get props => [
    patientId,
    message,
    name,
    roomNumber,
    bedNumber,
    roomAlertType,
    timeStamp
  ];

}
