import 'dart:convert';

import 'package:equatable/equatable.dart';

enum AdminType {
  notSet,
  nurse,
  maintenance,
}

class AdminTypeReadable {

  final AdminType adminType;
  final String adminTypeStr;

  AdminTypeReadable._internal(
    this.adminType,
    this.adminTypeStr,
  );

  factory AdminTypeReadable(AdminType mode) {
    switch (mode) {
      case AdminType.notSet:
        return AdminTypeReadable._internal(
          AdminType.notSet,
          'Select'
        );
      case AdminType.nurse:
        return AdminTypeReadable._internal(
          AdminType.nurse,
          'Nurse'
        );
      case AdminType.maintenance:
      return AdminTypeReadable._internal(
          AdminType.maintenance,
          'Maintenance'
        );
      default:
        throw Exception('Admin type not recognized');
    }
  }

  static serialize(String? adminTypeStr) {
    switch (adminTypeStr) {
      case 'Select': return AdminType.notSet;
      case 'Nurse': return AdminType.nurse;
      case 'Maintenance': return AdminType.maintenance;
      default: return AdminType.notSet;
    }
  }

}


class Admin extends Equatable {

  final String authId;
  final String? email, name, adminType, photo, notifToken;

  const Admin({
    required this.authId,
    this.email,
    this.name,
    this.adminType,
    this.photo,
    this.notifToken,
  });
  
  @override
  List<Object?> get props => [authId, email, name, adminType, photo, notifToken];

  static const empty = Admin(authId: '');

  bool get isEmpty => this == Admin.empty;

  factory Admin.fromJson(Map<String, dynamic> json) {
    return Admin(
      authId: json['authId'],
      email: json['email'],
      name: json['name'],
      adminType: json['adminType'],
      photo: json['photo'],
      notifToken: json['notifToken'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'authId': authId,
      'email': email,
      'name': name,
      'adminType': adminType,
      'photo': photo,
      'notifToken': notifToken
    };
  }

  String toStringJson() {
    final mapJson = toJson();
    return json.encode(mapJson);
  }

  static Map<String, dynamic> toMapJson(String deserJson) {
    return json.decode(deserJson);
  }

  Admin copyWith({
    String? authId,
    String? email,
    String? name,
    String? adminType,
    String? photo,
    String? notifToken,
  }) {
    return Admin(
      authId: authId ?? this.authId,
      email: email ?? this.email,
      name: name ?? this.name,
      adminType: adminType ?? this.adminType,
      photo: photo ?? this.photo,
      notifToken: notifToken ?? this.notifToken,
    );
  }

}
