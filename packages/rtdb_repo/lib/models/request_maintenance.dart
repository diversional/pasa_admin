class RequestMaintenance {

  String? timeStampAsId, patientId, message;
  int? type;

  RequestMaintenance({
    this.timeStampAsId = '',
    this.patientId = '',
    this.message = '',
    this.type = 0,
  });

  factory RequestMaintenance.fromJson(Map<String, dynamic> json) {
    return RequestMaintenance(
      timeStampAsId: json['timeStampAsId'],
      patientId: json['patientId'],
      message: json['message'],
      type: json['type'],
    );
  }

}
