enum RequestMaintenanceType {
  technician,
  housekeeping,
}

extension RequestMaintenanceTypeExtension on RequestMaintenanceType {

  int get type {
    switch (this) {
      case RequestMaintenanceType.technician:
        return 0;
      case RequestMaintenanceType.housekeeping:
        return 1;  
    }
  }

}