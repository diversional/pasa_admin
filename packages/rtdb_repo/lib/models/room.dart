import 'package:equatable/equatable.dart';

class Room extends Equatable {

  final String? number;
  final int? bedCount, floor;

  const Room({
    this.number = '',
    this.bedCount = 0,
    this.floor = 0,
  });
  
  @override
  List<Object?> get props => [number, bedCount, floor];

  static const empty = Room();

  bool get isEmpty => this == Room.empty;

  Map<String, dynamic> toJson() {
    return {
      'number': number,
      'bedCount': bedCount,
      'floor': floor,
    };
  }

}