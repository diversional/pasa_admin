import 'package:equatable/equatable.dart';

class ChatMessage extends Equatable {

  final String? id, body, senderAssignedId;

  ChatMessage({
    this.id = '',
    this.body = '',
    this.senderAssignedId = '',
  }) : super();

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'body': body,
      'senderAssignedId': senderAssignedId,
    };
  }

  factory ChatMessage.fromJson(Map<String, dynamic> json) {
    return ChatMessage(
      id: json['id'] ?? '',
      body: json['body'] ?? '',
      senderAssignedId: json['senderAssignedId'] ?? '',
    );
  }

  @override
  List<Object?> get props => [id, body, senderAssignedId];

  ChatMessage copyWith({
    String? id,
    String? body,
    String? senderAssignedId,
  }) {
    return ChatMessage(
      id: id ?? this.id,
      body: body ?? this.body,
      senderAssignedId: senderAssignedId ?? this.senderAssignedId,
    );
  }

  @override
  String toString() =>
      'ChatMessage { id: $id, body: $body, senderAssignedId: $senderAssignedId }';
}
