enum PendingRoomAlertType {
  assistance,
  emergency,
}

extension PendingRoomAlertTypeExtension on PendingRoomAlertType {

  int get type {
    switch (this) {
      case PendingRoomAlertType.assistance:
        return 0;
      case PendingRoomAlertType.emergency:
        return 1;
    }
  }

}