import 'package:firebase_database/firebase_database.dart';

class RealtimeDatabaseRef {

  static final RealtimeDatabaseRef _instance =
      RealtimeDatabaseRef._privateConstructor();

  RealtimeDatabaseRef._privateConstructor();

  factory RealtimeDatabaseRef() {
    return _instance;
  }

  static DatabaseReference _rootRef = FirebaseDatabase.instance.ref();

  // Admins
  final adminRoot = _rootRef.child('admins');
  DatabaseReference getAdmin(String authId) => adminRoot.child(authId);
  // Admins

  // Rooms
  final roomRoot = _rootRef.child('rooms');
  DatabaseReference getRoom(String roomNumber) => roomRoot.child(roomNumber);
  // Rooms

  // Patient
  final patientRoot = _rootRef.child('patients');
  /**
   * not to confuse Patient.id with authId which is set by FirebaseAuth
   */
  DatabaseReference getPatient(String authId) => patientRoot.child(authId);
  // Patient

  // Chat
  final chatRoot = _rootRef.child('chat_messages');

  DatabaseReference getPatientConversationsRoot(String patientId)
    => chatRoot.child(patientId);

  /**
   * Retrieves Message model
   *
   * not to confuse id with authId which is set by FirebaseAuth
   */
  DatabaseReference getAdminPatientConversations(String adminAuthId, String patientId)
    => getPatientConversationsRoot(patientId).child(adminAuthId);
  // Chat

  // PendingRoomAlert
  final alertRoot = _rootRef.child('pending_room_alerts');

  DatabaseReference getAlert(String patientId) => alertRoot.child(patientId);

  final historyAlertRoot = _rootRef.child('history_pending_room_alerts');
  DatabaseReference getHistoryPendingAlert(String id) => historyAlertRoot.child(id);
  // PendingRoomAlert

  // RequestMaintenance
  final technicianMaintenanceRoot = _rootRef.child('technician_maintenance_requests');
  final housekeepingMaintenanceRoot = _rootRef.child('housekeeping_maintenance_requests');

  DatabaseReference getTechnicianRequest(String timeStamp) =>
    technicianMaintenanceRoot.child(timeStamp);

  DatabaseReference getHousekeepingRequest(String timeStamp) =>
    housekeepingMaintenanceRoot.child(timeStamp);
  // RequestMaintenance

}
